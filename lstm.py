DS = ClassificationDataSet(2, class_labels=['Urd', 'Verdandi', 'Skuld'])
DS.appendLinked([ 0.1, 0.5 ]   , [0])
DS.appendLinked([ 1.2, 1.2 ]   , [1])
DS.appendLinked([ 1.4, 1.6 ]   , [1])
DS.appendLinked([ 1.6, 1.8 ]   , [1])
DS.appendLinked([ 0.10, 0.80 ] , [2])
DS.appendLinked([ 0.20, 0.90 ] , [2])

DS.calculateStatistics()
print DS.classHist
print DS.nClasses
print DS.getClass(1)
print DS.getField('target').transpose()
