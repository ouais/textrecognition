import IO
import os
import cv2
from IO import DatasetReader
import numpy as np
import preprocessing as pp
from preprocessing import Pipeline,normalize_sample,resize_sample
from generators import PatchGenerator
from learning import Experiment
from collections import Counter
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier
from test_word_recognition import Cascade
from misc import *
from pylab import *



def create_blacked_images():
	path = data_dir+'scenes/train/'

	pipeline = Pipeline()

	s1 = lambda x: pp.blacken_words(x)

	pipeline.add(s1)

	dio = DatasetReader(data_type=IO.TYPES[1],pipeline=pipeline)
	res = dio.read_images_with_boxes(path)
	images = [x.image for x in res]	
	
	IO.pickle(data_dir+"negative_images",images)

def read_lexicons(path):
    dirc = os.listdir(path)
    X = []
    dirc.sort()
    for fi in dirc:
    	f = open(path+fi,'r')
        words = f.readlines()
        words = [x.strip() for x in words]
        X.append(words)

    return X

def create_word_lexicons():
	path = data_dir+'scenes/test/'
	dio = DatasetReader(data_type=IO.TYPES[1])
	res = dio.read_images_with_boxes(path)
	counts = [len(x.tagged_rectangles)  for x in res ]

	scene_lexicons = read_lexicons('/home/ouais/data/lex/test/lex50/')
	word_lexicons = []

	for i in xrange(len(counts)):
		for j in xrange(counts[i]):
			word_lexicons.append(scene_lexicons[i][:])

	IO.pickle(word_lexicons,test_words_lex50_pkl)

def create_negative_chars_from_scenes():
	
	images = data_dir + "detector/negative_images.pkl"	
	images = IO.unpickle(images)
	images = images[0]
	sizes=[(11,11),(22,22),(33,33),(44,44),(55,55),(66,66),(77,77)]

	pg = PatchGenerator(sizes=sizes,n_patches=30000)
		
	patches = pg.generate(images)
	
	pipeline = Pipeline()	

	s1 = lambda x: [pp.togray(y) for y in x]
	s2 = lambda x: pp.toMat(x,(32,32),False)
	s3 = lambda x: pp.normalize(x)

	pipeline.add(s1,s2,s3)

	patches = pipeline.apply(patches)	

	IO.pickle(patches,data_dir+"detector/neg_char.pkl")
	
def create_trainingset_from_segmentation():
	X = IO.unpickle(natural_words_pkl)
	
	pipeline = Pipeline()	

	s1 = lambda x: [pp.togray(y) for y in x]
	s2 = lambda x: pp.toMat(x,(32,32),False)
	s3 = lambda x: pp.normalize(x)

	pipeline.add(s1,s2,s3)
	
	patches = []
	y = []
	for i in xrange(len(X)):
		cascade = Cascade(np.cumsum([0]+X[i].widths),mark_originals=True,split=True)
		intervals = cascade.intervals
		for interval in intervals:
			x = X[i].image[:,interval.start:interval.end]			
			patches.append(x)
			if interval.original:
				y.append(1)
			else:
				y.append(0)
	
	print Counter(y)

	patches = pipeline.apply(patches)

	save_dataset(X=patches)

	X = np.array(X)
	y = np.array(y)
	
	IO.pickle((patches,y),data_dir+'detection_segmentation.pkl')

def create_training_set():
	pos_chars = data_dir + 'detector/pos_char.pkl'
	neg_chars = data_dir + 'detector/neg_char.pkl'

	poss = IO.unpickle(pos_chars)[0]
	negs = IO.unpickle(neg_chars)
	# poss = pp.flatten(poss)
	# negs = pp.flatten(negs)

	# print len(poss[0])
	# print len(negs[0])

	X = np.concatenate((poss,negs))
	y = np.array([1]*len(poss) + [0]*len(negs))
	IO.pickle((X,y),data_dir+"detector/detection_set.pkl")

def create_haar_dataset():
	path = data_dir+"detector/detection_set.pkl"
	X,y = IO.unpickle(path)
	i = 0
	j = 0
	for k in xrange(len(X)):
		x = X[k].reshape(32,32)*255

		if y[k]:
			cv2.imwrite(data_dir+'haar/positive/%d.png' % i,x)
			i +=1
		else:
			cv2.imwrite(data_dir+'haar/negative/%d.png' % j,x)
			j += 1

def create_text_detector():
	negatives = data_dir + 'detector/neg_char.pkl'

	poss = IO.unpickle(natural_words_pkl)
	poss =[normalize_sample(resize_sample(x.image,(32,32))) for x in poss]

	negs = IO.unpickle(negatives)

	print len(poss)
	print len(negs)

	X = np.concatenate((poss,negs))
	y = np.array([1]*len(poss) + [0]*len(negs))
	IO.pickle((X,y),data_dir+"detector/detection_set.pkl")



def create_char_detector():

	e = Experiment()
	e.train_path = data_dir+"detector/text_detection.pkl"
	# e.mode = 'cv'
	# e.classifier = SVC(probability=True,C=2)
	e.classifier =ExtraTreesClassifier(n_estimators=30)
	e.run()
	print e.results
	IO.pickle(e.classifier,'detector_%s.pkl' % e.classifier.__class__.__name__)


if __name__ == '__main__':
	#create_trainingset_from_segmentation()
	create_word_lexicons()
	#create_blacked_images()
	#create_text_detector()
	#create_negative_chars_from_scenes()
	#create_training_set()
	# create_haar_dataset()
	#create_char_detector()
	

