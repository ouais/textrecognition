import IO
import numpy as np
from misc import *
from matplotlib import rc
import pylab as plt

plt.rc('text', usetex=False)
plt.rc('font', family='serif',size=6)

lm_order = range(2,9,1)
acc_small = [85.2,88.9,90.9,91.9,91.9,91.9,91.9]
acc_medium = [72.2,80.6,84.9,87.3,87.4,87.4,87.4]
acc_large = [69.0,75.9,80.2,83.0,84.1,84.3,84.3]
acc_large_star = [60.1,66.6,67.0,62.7,59.7,57.7,57.7]

plt.figure(figsize=(3.6,1.7))
plt.plot(lm_order,acc_small,'-o',linewidth=.5,markersize=3,label='Small')
plt.plot(lm_order,acc_medium,'-*',linewidth=.5,markersize=3,label='Medium')
plt.plot(lm_order,acc_large,'-^',linewidth=.5,markersize=3,label='Large')
plt.plot(lm_order,acc_large_star,'-x',linewidth=.5,markersize=3,label='Large*')

plt.legend(loc=7)

plt.xlabel('language model order')
plt.ylabel('accuracy (%)')
plt.savefig('language_model_effect.pdf',bbox_inches='tight')


beam_width = [1,5,10,15,20,50,100]
accuracy = [90.4,91.9,92,92.3,92.4,93.1,93.1]
speed = [.28,.4,.58,.78,.99,2.4,4.6]

fig, ax1 = plt.subplots(figsize=(3.4,1.5))
p1 = ax1.plot(beam_width, accuracy, '-o',label='Accuracy')
plt.ylim([85,100])
ax1.set_xlabel('beam width')
ax1.set_ylabel('accuracy (%)')

ax2 = ax1.twinx()
p2 = ax2.plot(beam_width, speed, '-hr',label='Speed')
ax2.set_ylabel('speed (seconds per query)')

plt.legend(p1+p2,['accuracy','speed'] ,loc=4)

plt.savefig('beam_effect.pdf',bbox_inches='tight')



plt.figure(figsize=(3.6,1.7))
pr_5 = IO.unpickle(data_dir+'pr_I_5.pkl')
pr_20 = IO.unpickle(data_dir+'pr_I_20.pkl')
pr_50 = IO.unpickle(data_dir+'pr_I_50.pkl')
pr_full = IO.unpickle(data_dir+'pr_I_full.pkl')
pr_large = IO.unpickle(data_dir+'pr_I_large.pkl')

plt.plot(pr_5[:,0],pr_5[:,1],'-+',linewidth=1,markersize=3,label='I-5')
plt.plot(pr_20[:,0],pr_20[:,1],'-D',linewidth=1,markersize=3,label='I-20')
plt.plot(pr_50[:,0],pr_50[:,1],'-*',linewidth=1,markersize=3,label='I-50')
plt.plot(pr_full[:,0],pr_full[:,1],'-x',linewidth=1,markersize=3,label='I-Full')
plt.plot(pr_large[:,0],pr_large[:,1],'-o',linewidth=1,markersize=3,label='I-Large')

plt.legend(loc=3)
plt.xlabel('precision')
plt.ylabel('recall')
plt.savefig('pr.pdf',bbox_inches='tight')