import IO
import time
import cv2
import preprocessing as pp
from preprocessing import *
from pylab import *
from bisect import bisect
import numpy as np
from misc import *




class CharDetector():

	def __init__(self):
		self.minwidth = 10
		self.step = 3
		self.detector = None
		self.recognizer = None
		self.threshold = .7
		self.delta = .0


	def NMS(self,M,lengths):
		#M = row_NMS(M,lengths)
		rows,cols = np.where(M > self.threshold)
		vals = [M[rows[i],cols[i]] for i in xrange(len(rows))]

		t = zip(rows,cols,vals)
		t = [list(x) for x in t]
		t.sort(key=lambda x:x[1])
		supress = [False] *len(t)

		for i in xrange(len(t)):
			j = i-1
			col_1 = t[i][1]
			while j > -1 and t[j][1]>= col_1-lengths[t[i][0]]/2 :
				if t[j][2] > t[i][2] + self.delta:				
					supress[i] = True
				elif t[j][2]+ self.delta <= t[i][2]:
					t[j][2] = 0

				j -= 1
			j = i+1
			while j < len(t) and t[j][1]<= col_1+lengths[t[i][0]]/2 :
				if t[j][2] > t[i][2]+ self.delta:
					supress[i] = True
				elif t[j][2]+ self.delta < t[i][2]:
					supress[j] = True
				j += 1

			if supress[i]:
				t[i][2] = 0

		z = filter(lambda x: x[2] > 0,t)
		z = [(lengths[x[0]],x[1],x[2]) for x in z]
		return z


	def NMS2(self,M,lengths):

		pr,pc = np.where(M >= self.threshold)
		nr,nc = np.where(M <= 1-self.threshold)

		pos = zip(pr,pc)
		pos.sort(key = lambda x: x[1])
		pr,pc = zip(*pos)

		neg = zip(nr,nc)
		neg.sort(key = lambda x: x[1])
		nr,nc = zip(*neg)

		dpp = np.array([M[pr[i],pc[i]]*lengths[pr[i]] for i in xrange(len(pr))])
		pprev = [-1]*len(dpp)
		dpn = np.array([0 for i in xrange(len(nr))])
		nprev = [-1]*len(dpn)

		i = 0
		j = 0		

		while i != len(pr) or j != len(nr):

			cur = 'char'
			if i == len(pr) or (j < len(nr) and nc[j] < pc[i]):
				cur = 'space'

			if cur == 'char':
				k = bisect(nc,pc[i]-1)
				k -=1
				mx = -1
				while k > -1:
					dist = abs(pc[i]-nc[k]-lengths[nr[k]]/2)
					a = mx == -1 or dpn[k]-(1-M[nr[k],nc[k]])*dist >= dpn[mx]
					b = pc[i]-nc[k] >= lengths[nr[k]]/2
					c = True
										
					if nprev[k] != -1:						
						c = pc[i]-pc[nprev[k]] >= (lengths[pr[i]]+lengths[pr[nprev[k]]])/2
					
					if a and b and c:						
						mx = k
					k -= 1

				if mx > -1:
					pprev[i] = mx
					dpp[i] += dpn[mx]
				i +=1
			else:
				k = bisect(pc,nc[j]-1)
				k -= 1
				mx = -1
				while k > -1:	
					dist = abs(nc[j]-pc[k]-lengths[pr[k]]/2)
					a = mx == -1 or dpp[k]-(1-M[nr[j],nc[j]])*dist >= dpp[mx]
					b = nc[j]-pc[k] >= lengths[pr[k]]/2
					c = True
										
					if pprev[k] != -1:						
						c = nc[j]-nc[pprev[k]] >= (lengths[nr[j]]+lengths[nr[pprev[k]]])/2

					if a and b and c:
						mx = k
					k -= 1

				if mx > -1:
					nprev[j] = mx
					dpn[j] += dpp[mx]
				j +=1

		k = dpn.argmax()
		cur = True
		z = []

		while k != -1:
			if cur == True:
				z.append((lengths[nr[k]],nc[k],M[nr[k],nc[k]]))
				k = nprev[k]
			else:
				z.append((lengths[pr[k]],pc[k],M[pr[k],pc[k]]))
				k = pprev[k]
			cur = not cur
		print z
		return z


	def NMS3(self,M,lengths,right_boundary= 199):
		pr,pc = np.where(M >= self.threshold)

		pos = zip(pr,pc)
		pos.sort(key = lambda x: x[1])
		pr,pc = zip(*pos)

		dpp = np.array([M[pr[i],pc[i]]*lengths[pr[i]]/2 for i in xrange(len(pr))])
		pprev = [-1]*len(dpp)

		i = 0

		while i != len(pr):

				k = i - 1
				mx = -1
				while k > -1:
					a = mx == -1 or dpp[k] >= dpp[mx]
					b = pc[i]-pc[k] >= (lengths[pr[i]]+lengths[pr[k]])/2
					
					if a and b:	
						mx = k
					k -= 1

				if mx > -1:
					pprev[i] = mx
					dpp[i] += dpp[mx]
				i +=1


		for x in xrange(len(dpp)):
			if pc[x] + lengths[pr[x]]/2 > right_boundary-5 and dpp[x] > dpp[k]:
				k = x
		z = []

		while k != -1:
			z.append((lengths[pr[k]],pc[k],M[pr[k],pc[k]]))
			k = pprev[k]
		print z
		return z



	def NMS4(self,M,E,lengths):

		pr,pc = np.where(M >= self.threshold)

		pos = zip(pr,pc)
		pos.sort(key = lambda x: x[1])
		pr,pc = zip(*pos)

		#dpp = np.array([M[pr[i],pc[i]]*lengths[pr[i]]/2 for i in xrange(len(pr))])
		dpp = np.array([E[pr[i],pc[i]] for i in xrange(len(pr))])
		dpp = dpp.T

		pprev = np.ones(size(dpp),dtype=int)*-1

		i = 0

		for i in xrange(dpp.shape[1]):
			for j in xrange(dpp.shape[0]):

				k = i - 1
				mx = -1
				while k > -1:
					for q in xrange(dpp.shape[0]):
						a = mx == -1 or dpp[k] >= dpp[mx]
						b = pc[i]-pc[k] >= (lengths[pr[i]]+lengths[pr[k]])/2					
						if a and b:	
							mx = (k,q)
					k -= 1

				if mx > -1:
					pprev[i,j] = mx
					dpp[i,j] += dpp[mx]


		for x in xrange(dpp.shape[1]):
			for j in xrange(dpp.shape[0]):
				if pc[x] + lengths[pr[x]]/2 > right_boundary-5:
					if (dpp[x,j] > dpp[k] or k == -1):
						k = (x,j)
		z = []

		while k != -1:
			z.append((lengths[pr[k]],pc[k],M[pr[k],pc[k]]))
			k = pprev[k]
		print z
		return z


	def plot_image(self,img,z):

		fig = figure()
		ax1 = fig.add_subplot(111)
		ax1.imshow(img, cmap = cm.Greys_r)
		ax2 = ax1.twinx()
		es,xs,cs = zip(*z)
		ys = [0]*len(xs)
		es = [x/2. for x in es]

		scatter_kwargs = {"zorder":100}

		ax2.errorbar(xs,ys,xerr=es,marker=None,fmt=None,ecolor='g')
		ax2.scatter(xs,ys,c=cs,cmap='OrRd',**scatter_kwargs)	
		show()
		
		
	def detect_chars(self,img):
		y = np.array([])

		m = img.shape[1]/2

		lengths = range(self.minwidth,m,self.step)

		a = np.zeros([len(lengths)*img.shape[1],1024])
		M = np.zeros([len(lengths),img.shape[1]])
		E = np.zeros([len(lengths),img.shape[1]],dtype = object)

		img_pad = np.pad(img,((0,0),(m/2,m/2)),'constant',constant_values=0)
		c = 0
		#M = IO.unpickle('M')
		#E = IO.unpickle('E')
		# k = 0
		# for i in xrange(M.shape[0]):
		# 	length = lengths[i]
		# 	for j in xrange(length/2,M.shape[1]-length/2):
		# 	#for j in xrange(M.shape[1]):
		# 			s = img_pad[:,(m-length)/2+j:(m+length)/2+j]
		# 			s = pp.resize_sample(s,(32,32))
		# 			s = pp.normalize_sample(s)
		# 			s = s.flatten()
		# 			a[k,:] = s
		# 			k += 1					

		# a = self.detector.predict_proba(a)
		a = IO.unpickle('A')
		#IO.pickle(a,'A')
		#print a[:].shape
		#M = a[:,1].reshape(M.shape)
		IO.pickle(M,'M')
		IO.pickle(E,'E')
		z = self.NMS2(M,lengths)	
		# print z

		self.plot_image(img,z)
		z.reverse()
		return z

	def get_chars(self,img):
		z = self.detect_chars(img)
		m = img.shape[1]/2
		img_pad = np.pad(img,((0,0),(m/2,m/2)),'constant',constant_values=0)

		chars = []
		k = 0 
		for char in z:
			if k % 2 == 1:
				c = img_pad[:,m/2+char[1]-char[0]/2:m/2+char[1]+char[0]/2]
				chars.append(c)
				c = pp.resize_sample(c,(32,32))
				c = pp.normalize_sample(c)
				c = c.flatten()   
				print ALPHABETi[self.recognizer.predict(c)[0]]
			k += 1
		return chars


	def get_activations(self,img,frame_width=10):
		
		img_pad = np.pad(img,((0,0),(frame_width/2,frame_width/2)),'constant',constant_values=0)
		A = np.zeros(img.shape[1])
		for i in xrange(img.shape[1]):
			patch = img_pad[:,i:i+frame_width]
			patch = pp.resize(patch,(32,32))
			patch = pp.normalize_sample(patch)
			patch = patch.flatten()       	
			A[i] = self.detector.predict_proba(patch)[0][1]

		#return A

		fig = figure()
		ax1 = fig.add_subplot(111)
		ax1.imshow(img, cmap = cm.Greys_r)
		ax2 = ax1.twinx()
		ax2.plot(A)
		show()    

def run_test():

	im_path = 'images/12.jpg'
	detector_path = 'svm.pkl'
	recognizer_path = 'recognizer_SVC.pkl'

	img = cv2.imread(im_path,cv2.CV_LOAD_IMAGE_COLOR)        
	img = togray(img)    
	img = cv2.resize(img,(200,img.shape[0]*200/img.shape[1]))
	img = normalize_sample(img)
	#cv2.imwrite('gray.jpg',img)
	#M = IO.unpickle('zzz.pkl')
	detector = IO.unpickle(detector_path)
	recognizer = IO.unpickle(recognizer_path)
	#imshow(img, cmap = cm.Greys_r)
	#show()
	cd = CharDetector()
	cd.detector = detector
	cd.recognizer = recognizer
	chars = cd.get_chars(img)


if __name__ == '__main__':
	run_test()
