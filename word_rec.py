from preprocessing import Pipeline,normalize_sample,togray

class Segmentation_WR():
	def __init_(self,segmentor,recognizer,detector):
		self.segmentor = segmentor
		self.detector = detector
		self.recognizer = recognizer

	def recognize(self,image):
		segments = self.segmentor.segment(image)
		cascade = Cascade(image,segments,self.detector,self.recognizer)


class Interval():
	def __init__(self,start,end,activation,detection):
		self.start = start
		self.end = end
		self.activation = activation
		self.detection = detection
		self.sorted_chars = [(x[1],ALPHABETi[x[0]]) for x in enumerate(activation)]
		self.sorted_chars.sort(reverse=True)
		self.cost = 0
		self.prev = -1

	def __repr__(self):
		return '('+str(self.start)+','+str(self.end)+') ' + 'conf: %.2f' % self.detection


class Node:
	def __init__(self,character,cost,interval):
		self.interval = interval
		self.character = character
		self.cost = cost
		self.prev = self

	def compute_trace(self):
		cur = self
		trace = []
		while cur.prev != cur:
			trace.append(cur.character)
			cur = cur.prev
		trace.append(cur.character)
		return ''.join(trace)


	def __repr__(self):
		return str(self.interval) +", " + " %.3f" % self.cost +", "+ str(self.character)


class Cascade:
	def __init__(self,image,segments,detector,recognizer):
		intervals = []
		pipeline = Pipeline()
		s1 = lambda x: resize_sample(x,(32,32))
		s2 = lambda x: normalize_sample(x)
		s3 = lambda x: x.flatten()
		pipeline.add(s1,s2,s3)

		for step in xrange(1,3):
			for i in xrange(len(segments)-step):
				t = pipeline.apply(image[:,segments[i]:segments[i+step]])			
				activation = recognizer.predict_proba(t)[0]
				detection = detector.predict_proba(t)
				interval = Interval(segments[i],segments[i+step],activation,detection[0][1])
				intervals.append(interval)

	
		self.intervals = intervals
		self.image = image
		self.length = image.shape[1]
		self.build_graph()

	def build_graph(self):

		ends = []
		self.intervals.sort(key=lambda x: x.end)

		for i in xrange(len(self.intervals)):
			ends.append(self.intervals[i].end)

		for i in xrange(len(self.intervals)-1,-1,-1):
			k = bisect(ends,self.intervals[i].start-1)
			self.intervals[i].prevs = []
			self.intervals[i].prev = i

			while k < len(self.intervals) and k != i and self.intervals[k].end == self.intervals[i].start:
				self.intervals[i].prevs.append(k)
				k +=1



def beamsearch(cascade,B=12,n_results=10):

	intervals = cascade.intervals
	end = cascade.length

	q = sortedlist([],lambda x:-x.cost)
	results = []

	for interval in intervals:
		print interval,interval.prevs
		if interval.end == end:			
			for c in interval.sorted_chars:						
				if len(q)< B or q[-1].cost < c[0]:
					q.add(Node(c[1],c[0],interval))
				if len(q) > B:
					del q[-1]

	while len(q) != 0:
		x = q[0]
		del q[0]
		if x.interval.start == 0:
			results.append(x.compute_trace())
			if len(results) == n_results:
				break

		for j in x.interval.prevs:
			interval = intervals[j]
			for c in interval.sorted_chars:
				if len(q)< B or q[-1].cost < c[0]:
					u = Node(c[1],c[0],interval)
					u.prev = x
					q.add(u)
				if len(q) > B:
					del q[-1]
	print results