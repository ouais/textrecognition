import sys
import numpy as np
import scipy
import IO
import Image
import cv2
from sklearn import hmm
from hybridhmm import *
from estimators import WindowEstimator
from pylab import *
import preprocessing as pp
from preprocessing import Pipeline,normalize_sample,togray
from IO import DatasetReader
from detector import CharDetector
from datetime import datetime
from misc import *
from bisect import *
from blist import sortedlist

image_dir = data_dir + "words/train/"
image_pkl  = data_dir + "train_words.pkl"
activations_pkl  = data_dir + "word_activations.pkl"
detector_path = 'svm.pkl'


class HMMSegmentor():
	def __init__(self,path=None):
		if path != None:
			self.load(path)

	def load(self,path):
		self.model = IO.unpickle(path)

	def fit(self,X):
		n_classes = 2
		spc = 4
		A,pi = left_to_right_discrimnative(n_classes,spc)
		model = Hybrid_Discrimnative_HMM(n_classes=n_classes,spc=spc,transmat=A,startprob=pi,n_iter=10)
		model.estimator = WindowEstimator(n_classes=n_classes*spc,frame_width=10,evaluate=True)
		model.fit(X)
		self.model = model

		IO.pickle(model,"segmentation_hmm.pkl")

	def segment_raw(self,img):
		simg = SequencedImage(img,None,self.model.estimator,caching=True)
		seq = self.model.segment(simg)
		return seq


	def segment(self, img):
		simg = SequencedImage(img,None,self.model.estimator,caching=True)
		seq = self.model.segment(simg)
		segments = []
		color = 1
		prev = 0
		for i in xrange(len(seq)-1):
			if seq[i] != seq[i+1]:
				segments.append([i-prev+1,color])
				color = 1-color
				prev = i+1
		segments.append([len(seq)-1-prev,color])
		
		for i in xrange(len(segments)):
			if segments[i][1] == 0:
				if i > 0:
					segments[i-1][0] += int(np.floor(segments[i][0]/2.))
				if i < len(segments)-1:
					segments[i+1][0] += int(np.ceil(segments[i][0]/2.))
		
		segments = filter(lambda x: x[1] == 1,segments)
		
		segments = [0] + [x[0] for x in segments]
		#print np.cumsum(segments)
		return np.cumsum(segments)

def createHMM():
	pi = np.array([0.5, 0.5])
	A = np.array([[0.7, 0.3], [0.3, 0.7]])

	means = np.array([[0.0], [.9]])
	covars = np.tile(np.identity(1), (2, 1, 1))
	model = hmm.GaussianHMM(2, "full", pi, A)
	model.means_ = means
	model.covars_ = covars
	X, Z = model.sample(100)


def create_image_pkl():

	pipeline = Pipeline()	

	s1 = lambda dataset: ([(pp.togray(img),tag) for img,tag in dataset])	
	s2 = lambda dataset: ([(pp.resize_to_width(img,200),tag) for img,tag in dataset])
	s3 = lambda dataset: ([(pp.normalize_sample(img),tag) for img,tag in dataset])	

	pipeline.add(s1,s2,s3)

	dio = DatasetReader(pipeline=pipeline)
	dio.create_pickle(image_dir,image_pkl)


def create_activations_pkl():
	
	images = IO.unpickle(image_pkl)
	detector = IO.unpickle(detector_path)
	cd = CharDetector()
	cd.detector = detector
	image_activations = [(img,cd.get_activations(img),tag) for img,tag in images]

	IO.pickle(activations_pkl,image_activations)	


def playwithhmms():
	pi = np.array([0.5, 0.5])
	A = np.array([[0.7, 0.3], [0.3, 0.7]])
	B = np.array([[0.9, 0.1], [0.1, 0.9]])

	model = hmm.MultinomialHMM(n_components=2,transmat=A,startprob=pi)
	model.emissionprob_ = B
	IO.pickle(model,'meh')
	#print model._log_emissionprob
	x =[0,0,0,0,1,1,1,1]
	#print model.eval(x)
	print model._compute_log_likelihood(x)
	#print model.decode(x,algorithm='map')
	X = []
	X.append([2.,2.,2.,1.,1.,1.,1.])
	X.append([3.,3.,1.,1.,1.])
	model.fit(X)


def left_to_right(n_states,x=2):
	A = np.zeros([n_states,n_states])
	
	for row in xrange(n_states):
		A[row,:] = np.array([0.]*row + [x**(n_states-row-r) for r in xrange(n_states-row)])
		#A[row,:] = np.array([0.]*row + [1]*(n_states-row))
		A[row,:] /= A[row,:].sum()

	pi = np.array([x**(n_states-r) for r in xrange(n_states)],dtype='float')
	pi /= pi.sum()

	return A,pi


def left_to_right_discrimnative(n_classes,spc,x=2,limit=None): #spc: states per class
	Z = np.zeros([spc,spc])
	A = np.zeros([n_classes*spc,n_classes*spc])
	
	if limit == None:
		limit = spc

	for row in xrange(spc):
		Z[row,row:min(row+limit,spc)] = np.array([x**(spc-r) for r in xrange(min(row+limit,spc)-row)])
		Z[row,:] /= Z[row,:].sum()

	for c in xrange(n_classes):
		A[spc*c:spc*(c+1),spc*c:spc*(c+1)] = Z

	pi = np.array([x**(spc-r) for r in xrange(spc)],dtype='float')
	
	pi = np.tile(pi,n_classes)
	pi /= pi.sum()

	return A,pi

def segmentation_transition(spc,x=2,limit=None):
	Z = np.zeros([spc,spc])
	A = np.zeros([2*spc,2*spc])
	
	if limit == None:
		limit = spc

	for row in xrange(spc):
		Z[row,row:min(row+limit,spc)] = np.array([x**(spc-r) for r in xrange(min(row+limit,spc)-row)])
		Z[row,:] /= Z[row,:].sum()

	for c in xrange(2):
		A[spc*c:spc*(c+1),spc*c:spc*(c+1)] = Z

	A[spc-1,spc-1] = x**2
	A[spc-1,spc] = x

	A[2*spc-1,2*spc-1] = x**2
	A[2*spc-1,0] = x

	A /= A.sum(axis=1)[:,np.newaxis]	
	
	pi = [0]*2*spc
	pi[spc] = 1.

	return A,pi

def test_hmm_on_words():
	
	dataset = IO.unpickle(word_train_pkl)
	words,tags = dataset[0],dataset[1]
	
	tags = [[iALPHABET[j] for j in tags[i]] for i in xrange(len(tags))]

	n_classes= 62
	spc = 4
	A,pi = left_to_right_discrimnative(n_classes,spc)
	mask = A > 0
	model = Hybrid_Discrimnative_HMM(n_classes=n_classes,spc=spc,transmat=A,startprob=pi,n_iter=10)
	model.estimator = WindowEstimator(n_classes=n_classes*spc,frame_width=5,evaluate=True)
	model.fit(zip(words,tags))			

def segmentation_hmm():
	#dataset = IO.unpickle(synthetic_words_pkl)
	dataset = IO.unpickle(natural_words_pkl)
	dataset = dataset[:100]

	n_classes = 2
	spc = 4
	A,pi = left_to_right_discrimnative(n_classes,spc)
	model = Hybrid_Discrimnative_HMM(n_classes=n_classes,spc=spc,transmat=A,startprob=pi,n_iter=5)
	model.estimator = WindowEstimator(n_classes=n_classes*spc,frame_width=10,evaluate=True)
	model.fit(dataset)
	IO.pickle(model,"maxout_natural_hmm_10.pkl")


def testhdh():
	q = {}
	d1 = IO.unpickle(data_dir + "pickles/_c__all_2.pkl")
	d2 = IO.unpickle(data_dir + "pickles/_c__all_3.pkl")

	d1 = d1[0:100]
	d2 = d2[0:100]

	q[d1[0][1]] = len(q)
	q[d2[0][1]] = len(q)

	d1 = [(x[0],[q[x[1]]]) for x in d1]
	d2 = [(x[0],[q[x[1]]]) for x in d2]

	print len(d1),len(d2)

	d1.extend(d2)
	n_classes= 2
	spc = 4
	A,pi = left_to_right_discrimnative(n_classes,spc)
	mask = A > 0
	model = Hybrid_Discrimnative_HMM(n_classes=n_classes,spc=spc,mask=mask,transmat=A,startprob=pi,n_iter=10,post_m=test23)
	model.estimator = WindowEstimator(n_classes=n_classes*spc,frame_width=5,evaluate=True)

	model.fit(d1)
	IO.pickle(model,'2_3_hmm.pkl')
	model = IO.unpickle('2_3_hmm.pkl')
	np.set_printoptions(suppress=True,precision=2)
	print model.transmat_
	test23(model)
	
def testhdh_allchars():
	q = {}
	training_set = []

	classes = '0123456789'
	for char in classes:
		d = IO.unpickle(data_dir + "pickles/_c__"+char+".pkl")
		q[char] = len(q)
		d = [(x[0],q[x[1]]) for x in d]		
		training_set.extend(d)

	n_classes= len(classes)
	spc = 4
	A,pi = left_to_right_discrimnative(n_classes,spc)
	mask = A > 0
	model = Hybrid_Discrimnative_HMM(n_classes=n_classes,spc=spc,mask=mask,transmat=A,startprob=pi,n_iter=10,post_m=test_all)
	model.estimator = WindowEstimator(n_classes=n_classes*spc,frame_width=5,evaluate=True)
	#print training_set
	model.fit(training_set)
	IO.pickle(model,'hmmmodel_all.pkl')
	model = IO.unpickle('hmmmodel_all.pkl')

	

def test_all(model):
	testing_set = []
	classes = '0123456789'
	q = {}
	for char in classes:
		d = IO.unpickle(data_dir + "pickles/_c__test"+char+".pkl")		
		q[char] = len(q)
		d = [(x[0],q[x[1]]) for x in d]		
		testing_set.extend(d)

	hits = 0
	for x in testing_set:
		res = model.decode_to_classes(x[0])
		if res[0] == x[1]:
			hits +=1
		else:
			pass	

	print hits,len(testing_set),hits*1./len(testing_set)

def testtrain23(model):

	dt1 = IO.unpickle(data_dir + "pickles/_c__2.pkl")
	dt2 = IO.unpickle(data_dir + "pickles/_c__3.pkl")

	q = {}

	q[dt1[0][1]] = len(q)
	q[dt2[0][1]] = len(q)

	print len(dt1),len(dt2)

	dt1.extend(dt2)
	hits = 0
	np.set_printoptions(suppress=True,precision=3)

	for x in dt1:
		res = model.decode_to_classes(x[0])
		
		if res[0] == q[x[1]]:
			hits +=1
		else:
			pass
			#print res,q[x[1]]
			#print model.decode(x[0])			
			#print model._compute_log_likelihood(x[0])

	print hits,len(dt1),hits*1./len(dt1)


def test23(model):

	dt1 = IO.unpickle(data_dir + "pickles/_c__test2.pkl")
	dt2 = IO.unpickle(data_dir + "pickles/_c__test3.pkl")

	q = {}

	q[dt1[0][1]] = len(q)
	q[dt2[0][1]] = len(q)

	print len(dt1),len(dt2)

	dt1.extend(dt2)
	hits = 0
	chits = [0,0]
	np.set_printoptions(suppress=True,precision=2)
	i = 0
	for x in dt1:
		t= SequencedImage(x[0],None,model.estimator,caching=True)
		res = model.decode_to_classes(t)
		
		if res[0] == q[x[1]]:
			hits +=1
			chits[res[0]] +=1
		else:
			# scipy.misc.imsave('log/misclass_%d.png' % i, x[0])
			# print res,q[x[1]]
			# print model.decode(x[0])			
			# print np.exp(model._compute_log_likelihood(x[0]))
			i +=1

	print hits,len(dt1),chits,hits*1./len(dt1)
	sys.stderr.write(
		str(hits)+","+
		str(len(dt1))+","+
		str(chits)+","+
		str(hits*1./len(dt1))+"\n")


def testhybridhmm():

	d1 = IO.unpickle(data_dir + "pickles/_c__2.pkl")
	d2 = IO.unpickle(data_dir + "pickles/_c__3.pkl")

	
	h2 = train_hmm(data_dir + "pickles/_c__2.pkl")
	h3 = train_hmm(data_dir + "pickles/_c__3.pkl")

	
	dt1 = IO.unpickle(data_dir + "pickles/_c__test2.pkl")
	dt2 = IO.unpickle(data_dir + "pickles/_c__test3.pkl")


	print h2.score(dt1[0][0])
	print h3.score(dt1[0][0])

	print h2.score(dt2[0][0])
	print h3.score(dt2[0][0])	


def trainallcharhmms():
	for char in misc.ALPHABET:
		specific_pickle(char)

	# hmmlist = []
	# for char in misc.ALPHABET:
	# 	model = train_hmm(data_dir + "pickles/_c__"+char+".pkl")
	# 	hmmlist.append(model)
	# 	IO.pickle(model,data_dir+"pickles/_hmm__"+char+".pkl")
	# IO.pickle(hmmlist,data_dir+'pickles/hmmlist.pkl')



def specific_pickle(chars):
	trainpath = data_dir + "chars/train/"
	trainpickle  = data_dir + "pickles/_c__all_"+chars+".pkl"	
	pipeline = Pipeline()

	s1 = lambda x: map(lambda y: (pp.togray(y[0]),y[1]),x)
	s2 = lambda x: map(lambda y: (pp.normalize_sample(y[0]),y[1]),x)

	pipeline.add(s1,s2)

	dr = IO.DatasetReader(files='all',tags = chars)
	dr.pipeline = pipeline

	dr.create_pickle(trainpath,trainpickle)


def show_sample():
	image_activations = IO.unpickle(activations_pkl)
	x = 20#np.random.randint(len(image_activations))
	img,activations,tag = image_activations[x]

	fig = figure()
	ax1 = fig.add_subplot(111)
	ax1.imshow(img, cmap = cm.Greys_r)
	ax2 = ax1.twinx()
	ax2.plot(activations)
	show() 


def test_char_pkl():
	trainpickle  = data_dir + "_c__.pkl"

	p = IO.unpickle(trainpickle)
	IO.pickle(p[0:10],data_dir + "_c__2.pkl")
	print len(p)
	imshow(p[25][0])
	show()


if __name__=='__main__':

	#sys.stdout = open('log/'+str(datetime.now()), 'w')
	#testhdh()
	#test_hmm_on_words()
	segmentation_hmm()
	#testhdh_allchars()
	#specific_pickle('2')
	#specific_pickle('3')

	#create_image_pkl()
	#create_activations_pkl()
	#playwithhmms()
	#pecific_pickle()
	#test_char_pkl()
	#testhybridhmm()
	#hmm = createHMM()
	#show_sample()
	#testhybridhmm()
	#trainallcharhmms()
	#sys.stdout.close()