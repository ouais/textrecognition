import numpy as np
import matplotlib.cm as cm
from preprocessing import togray,normalize_sample,center_sample
from matplotlib.pyplot import *
import matplotlib.image as mpimg
from skimage.transform import PiecewiseAffineTransform, warp, swirl
from numpy.random import randint,random
from scipy import ndimage
from skimage import data


def t_sin_wave(img):
	rows, cols = img.shape[0], img.shape[1]

	src_cols = np.linspace(0, cols, cols/20)
	src_rows = np.linspace(0, rows, rows/30)
	src_rows, src_cols = np.meshgrid(src_rows, src_cols)
	src = np.dstack([src_cols.flat, src_rows.flat])[0]

	# add sinusoidal oscillation to row coordinates
	pis = (randint(2,10)*np.pi,randint(2,10)*np.pi)
	freqs = (img.shape[0]/(randint(20,50)),img.shape[1]/(randint(20,50)))
	dst_rows = src[:, 1] - np.sin(np.linspace(0, pis[0], src.shape[0])) * freqs[0]
	dst_cols = src[:, 0] - np.sin(np.linspace(0, pis[1], src.shape[0])) * freqs[1]
	dst = np.vstack([dst_cols, dst_rows]).T


	tform = PiecewiseAffineTransform()
	tform.estimate(src, dst)

	out_rows = img.shape[0]
	out_cols = cols
	out = warp(img, tform, output_shape=(out_rows, out_cols))

	return out


def t_swirl(img):
	swirled = swirl(img, rotation=20, strength=.2, radius=10, order=1)
	return swirled

def t_noise(img):
	return img+random(img.shape)*img.max()/randint(1,30)

def t_gaussian_blur(img):
	return ndimage.gaussian_filter(img, sigma=randint(0,2)*2+1)



if __name__=='__main__':
	path = 'images/1.jpg'
	img=mpimg.imread(path)
	img = togray(img)
	img = center_sample(img)
	img = t_sin_wave(img)		
	img = t_gaussian_blur(img)
	img = t_noise(img)
	imshow(img,origin='lower',cmap = cm.Greys_r)
	show()