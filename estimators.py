import numpy as np
import preprocessing as pp
from sklearn.linear_model import SGDClassifier
from PipelineClassifier import PipelineClassifier
from sklearn.ensemble import ExtraTreesClassifier
from preprocessing import resize_sample,normalize_sample,Pipeline
from sklearn.svm import SVC
from collections import Counter
from wrappers import MaxoutWrapper
#from cnn import CNN

class _BaseEstimator():

	def __init__(self):
		pass

	def compute_state_posteriors(self,img):
		pass

	def update_batch(self,obs):
		pass


class WindowEstimator(_BaseEstimator):

	def __init__(self,frame_width=10,input_size=(32,32),n_classes=None,evaluate=False):

		self.n_classes = n_classes
		self.frame_width = frame_width
		self.pc = PipelineClassifier()
		#self.pc.classifier = ExtraTreesClassifier(n_estimators=100)
		self.pc.classifier = MaxoutWrapper()
		self.pc.classifier.yamlf = 'yaml/hmm_maxout_2.yaml'
		#self.pc.classifier = SVC(probability=True)
		#self.pc.classifier = CNN(n_classes=n_classes)

		self.evaluate = evaluate
		self.fit_id = 0

		pipeline = Pipeline()

		s1 = lambda x: resize_sample(x,input_size)
		s2 = lambda x: normalize_sample(x)
		s3 = lambda x: x.flatten()

		pipeline.add(s1,s2)

		self.pc.pipeline = pipeline
		self.n_classes = n_classes

	def slicer_xrange(self,img):
		#return xrange(self.frame_width/2,img.shape[1]-self.frame_width/2,self.frame_width)
		#return xrange(self.frame_width/2,img.shape[1]-self.frame_width/2)
		return xrange(img.shape[1])

	def slicer(self,img):
		#self.frame_width = img.shape[1]/20
		img_pad = np.pad(img,((0,0),(self.frame_width/2,self.frame_width/2)),'constant',constant_values=0)
		for i in self.slicer_xrange(img):
			#patch = img[:,max(i-self.frame_width/2,0):min(i+self.frame_width/2,img.shape[1])]
			patch = img_pad[:,i:i+self.frame_width]
			yield patch


	def predict(self,obs):
		if obs.posterior_id == self.fit_id:
			return obs.posteriors

		patches = obs.sequence
		posteriors = np.zeros((len(patches),self.n_classes))
		partial_posteriors = self.pc.predict(patches,batch=True,preprocess= not obs.caching)

		if partial_posteriors.shape != posteriors.shape:
			for i in xrange(len(self.uniques)):
				posteriors[:,self.uniques[i]] = partial_posteriors[:,i]
		else:
			posteriors = partial_posteriors

		obs.posteriors = posteriors
		obs.posterior_id = self.fit_id
		return obs.posteriors


	def update(self,obs):

		X = []
		y = []
		for img,seq in obs:
			X.extend(img.sequence)
			y.extend(seq)

		X = np.array(X)
		y = np.array(y)
		print "fitting %d samples with %s..." % (len(y),self.pc.classifier.__class__.__name__)
		self.uniques = np.unique(y)
		self.fit_id += 1
		self.pc.classifier.fit(X,y)
		if self.evaluate:
			return self.pc.classifier.score(X,y)
		else:
			return 0



class SequencedImage:
	def __init__(self,img,labels,estimator,caching=None):
		self.image = img
		self.estimator = estimator
		self.caching = caching
		self.labels = labels
		self.posterior_id = -1
		if self.caching:
			self._sequence = np.array([self.estimator.pc.preprocess(y) for y in self.estimator.slicer(img)])


	def _get_seq_length(self):
		return len(self.sequence)

	def _get_seq(self):
		if self.caching:
			return self._sequence
		else:
			return [y for y in self.estimator.slicer(self.image)]


	length = property(_get_seq_length)
	sequence = property(_get_seq)


class CDWindowEstimator(_BaseEstimator):

	def __init__(self,frame_width=10,input_size=(32,32),n_classes=None,mask=None,spc=None):

		self.n_classes = n_classes
		self.frame_width = frame_width
		
		pipeline = Pipeline()

		s1 = lambda x: resize(x,input_size)
		s2 = lambda x: normalize_sample(x)
		s3 = lambda x: x.flatten()

		pipeline.add(s1,s2,s3)

		self.n_classes = n_classes
		self.spc = spc
		self.pcs = []
		self.priors = []

		for i in xrange(self.n_classes+1):
			pc = PipelineClassifier()
			pc.classifier = SVC(probability=True)
			pc.pipeline = pipeline
			self.pcs.append(pc)
			self.priors.append([])

		if mask == None:
			mask = np.ones([self.n_classes+1,self.n_classes+1],dtype=int)


	def slicer_xrange(self,img):
		return xrange(self.frame_width/2,img.shape[1]-self.frame_width/2,self.frame_width)
		#return xrange(img.shape[1])

	def slicer(self,img):
		
		#img_pad = np.pad(img,((0,0),(self.frame_width/2,self.frame_width/2)),'constant',constant_values=1)
		for i in self.slicer_xrange(img):
			patch = img[:,max(i-self.frame_width/2,0):min(i+self.frame_width/2,img.shape[1])]
			#patch = img_pad[:,i:i+self.frame_width]
			yield patch


	def _compute_std_posterior(self,index,patch):
		posterior = np.zeros(self.n_classes)
		t = self.pcs[index].predict(patch)
		for i in xrange(len(t[0])):
			posterior[self.uniques[index][i]] = t[0][i]

		return posterior


	def compute_state_posteriors(self,img):		

		posteriors = []

		firstpass = True
		prev_posterior = np.zeros(self.n_classes)
		for patch in self.slicer(img):
			if firstpass:
				cur_posterior = self._compute_std_posterior(0,patch)
				#print cur_posterior
			else:
				cur_posterior = np.zeros(self.n_classes)
				for j in xrange(len(cur_posterior)):
					if prev_posterior[j] > 0:
						cur_posterior += prev_posterior[j]*self._compute_std_posterior(j+1,patch)

			firstpass = False
			posteriors.append(cur_posterior)
			prev_posterior = cur_posterior

		return np.array(posteriors)


	def update(self,img,state_seq):
		pass

	def update_batch(self,obs):
		Xs = [[] for i in xrange(self.n_classes+1)]
		ys = [[] for i in xrange(self.n_classes+1)]
		self.uniques = [[] for i in xrange(self.n_classes+1)]

		for img,seq in obs:
			i = 0
			for patch in self.slicer(img):
				if i == 0:
					Xs[0].append(self.pcs[0].preprocess(patch))
					ys[0].append(seq[i])
				else:
					Xs[seq[i-1]+1].append(self.pcs[seq[i-1]+1].preprocess(patch))
					ys[seq[i-1]+1].append(seq[i])
				i+=1
				# Xs[0].append(self.pcs[0].preprocess(patch))
				# ys[0].append((seq[i]/self.spc)*self.spc)
				# if i >0:
				# 	Xs[seq[i-1]+1].append(self.pcs[seq[i-1]+1].preprocess(patch))				
				# 	ys[seq[i-1]+1].append(seq[i])
				# i+=1

		
		for i in xrange(self.n_classes+1):
			if Xs[i] == []:
				continue
			
			c = Counter(ys[i])
			print i,c

			# self.priors[i] = np.array([c[x]+1. for x in xrange(self.n_classes)])
			# self.priors[i] /= self.priors[i].sum()
			self.uniques[i] = np.unique(ys[i])
			self.pcs[i].fit(Xs[i],ys[i])



