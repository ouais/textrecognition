import IO
from detector import CharDetector
from recognizer import CharRecognizer
from preprocessing import *
from IO import DatasetReader
from hmms import HMMSegmentor
from misc import *
from PipelineClassifier import PipelineClassifier

detector_cls_pkl = 'trees_frame.pkl'
recognizer_cls_pkl = 'recognizer.pkl'

def create_word_recognition_pkl():
	pipeline = Pipeline()
	s1 = lambda x: zip(*x)
	s2 = lambda x: ([togray(t) for t in x[0]],x[1])
	s3 = lambda x: ([normalize_sample(t) for t in x[0]],x[1])

	pipeline.add(s1,s2,s3)

	dio = DatasetReader(pipeline=pipeline, files=['all'])
	dio.create_pickle(word_train_dir,word_train_pkl)


def test_word_recognition():
	pipeline = Pipeline()

	s1 = lambda x: resize(x,(32,32))
	s2 = lambda x: normalize_sample(x)
	s3 = lambda x: x.flatten()

	pipeline.add(s1,s2,s3)

	detector_cls = IO.unpickle(detector_cls_pkl)
	recognizer_cls = IO.unpickle(recognizer_cls_pkl)
	detector = CharDetector()
	recognizer = CharRecognizer()

	detector.classifier = detector_cls
	recognizer.classifier = recognizer_cls
	recognizer.pipeline = pipeline

	dataset = IO.unpickle(test_pkl)
	words,tags = dataset[0],dataset[1]

	for i in xrange(len(words)):
		word = words[i]

		tag = tags[i]
		chars = detector.get_chars(word)
		s = ''
		for char in chars:
			s += chr(recognizer.classify(char))
		print s,tag

def test_single_word_recognition():
	im_path = 'images/7.jpg'
	word = cv2.imread(im_path,cv2.CV_LOAD_IMAGE_COLOR)        
	word = togray(word)    
	word = cv2.resize(word,(200,word.shape[0]*200/word.shape[1]))
	word = normalize_sample(word)

	pipeline = Pipeline()

	s1 = lambda x: resize(x,(32,32))
	s2 = lambda x: normalize_sample(x)
	s3 = lambda x: x.flatten()

	pipeline.add(s1,s2,s3)

	detector_cls = IO.unpickle(detector_cls_pkl)
	recognizer_cls = IO.unpickle(recognizer_cls_pkl)
	detector = CharDetector()
	recognizer = PipelineClassifier()

	detector.classifier = detector_cls
	recognizer.classifier = recognizer_cls
	recognizer.pipeline = pipeline

	chars = detector.get_chars(word)
	s = ''
	for char in chars:
		# imshow(char)
		# show()
		print recognizer.predict(char)
	print s


if __name__=='__main__':
	test_single_word_recognition()
	# detector_cls = IO.unpickle(detector_pkl)
	# recognizer_cls = IO.unpickle(recognizer_pkl)
	# detector = CharDetector()
	#create_word_recognition_pkl()