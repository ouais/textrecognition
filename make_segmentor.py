import IO
import cv2
import numpy as np
from misc import *
from pylab import *
from preprocessing import resize_sample,normalize_sample,Pipeline,togray,resize_to_width
from sklearn.ensemble import ExtraTreesClassifier

class NMS_Segmentor:
	def __init__(self,classifier_path):
		self.classifier  = IO.unpickle(classifier_path)
		self.p = Pipeline()

		s1 = lambda x: resize_sample(x,(32,32))
		s2 = lambda x: normalize_sample(x)
		s3 = lambda x: x.flatten()
		self.p.add(s1,s2,s3)

		self.frame_width = 10

	def segment(self,img):

		X = [self.p.apply(x) for x in slicer(img,self.frame_width)]
		A = classifier.predict_proba(X)[:,0]
		A = self.NMS(A)
		plot_activation(img,A)


	def NMS(self,A):
		for i in xrange(len(A)):
			for j in xrange(max(0,i-self.frame_width),i):
				if A[i] >= A[j]:
					A[j] = 0
				elif A[i] < A[j]:
					A[i] = 0
		return A

def slicer(img,frame_width):
		img_pad = np.pad(img,((0,0),(frame_width/2,frame_width/2)),'constant',constant_values=0)
		for i in xrange(img.shape[1]):
			#patch = img[:,max(i-self.frame_width/2,0):min(i+self.frame_width/2,img.shape[1])]
			patch = img_pad[:,i:i+frame_width]
			yield patch


def build_segmentor():
	S = IO.unpickle(natural_words_pkl)
	classifier = ExtraTreesClassifier(n_estimators=50)

	y = []
	X = []

	p = Pipeline()

	s1 = lambda x: resize_sample(x,(32,32))
	s2 = lambda x: normalize_sample(x)
	s3 = lambda x: x.flatten()
	p.add(s1,s2,s3)

	X = []
	y = []
	for s in S:
		sequence = [p.apply(x) for x in slicer(s.image,10)]
		X.extend(sequence)
		for segment in s.segments:			
			y.extend([segment[1]]*segment[0])


	X = X[:50000]
	y = y[:50000]

	classifier.fit(X,y)

	IO.pickle(classifier,classifier.__class__.__name__+'_segmentor.pkl')


def NMS(A,frame_width):
		for i in xrange(len(A)):
			for j in xrange(max(0,i-frame_width),i):
				if A[i] >= A[j]:
					A[j] = 0
				elif A[i] < A[j]:
					A[i] = 0
		return A

def plot_activation(img,A):

	fig = figure()
	ax1 = fig.add_subplot(111)
	ax1.imshow(img, cmap = cm.Greys_r)
	ax2 = ax1.twinx()
	ax2.plot(A)
	show()

def test_segmentor():
	img = cv2.imread('images/11.jpg')
	img = togray(img)
	img = resize_to_width(img,200)

	classifier  = IO.unpickle('ExtraTreesClassifier_segmentor.pkl')
	frame_width = 10

	p = Pipeline()

	s1 = lambda x: resize_sample(x,(32,32))
	s2 = lambda x: normalize_sample(x)
	s3 = lambda x: x.flatten()
	p.add(s1,s2,s3)

	X = [p.apply(x) for x in slicer(img,frame_width)]
	A = classifier.predict_proba(X)[:,0]
	A = NMS(A,frame_width)
	plot_activation(img,A)


if __name__ =='__main__':
	#build_segmentor()
	test_segmentor()