import IO
import cv2
import numpy as np
from preprocessing import resize_sample,Pipeline,normalize_sample,togray
from pylab import imshow,matshow,show
from misc import *
from IO import *
from wrappers import *


def read_dataset():
	path = data_dir+'scenes/train/'

	pipeline = Pipeline()

	s1 = lambda x: pp.blacken_words(x)

	pipeline.add(s1)

	dio = DatasetReader(data_type=IO.TYPES[1],pipeline=pipeline)
	res = dio.read_images_with_boxes(path)
	print len(res)


def test_detection_single_image(img=None):
	size = 32
	if img == None:
		im_path = 'images/s1.jpg'
		img = cv2.imread(im_path)
		img = togray(img)
	#detector = IO.unpickle('detector_SVC.pkl')
	detector = MaxoutWrapper('yaml/detector.pkl')


	img_pad = np.pad(img,size/2,'constant',constant_values=0)
	A = np.zeros(img.shape)

	pipeline = Pipeline()

	s1 = lambda x: resize_sample(x,(size,size))
	s2 = lambda x: normalize_sample(x)
	s3 = lambda x: x.flatten()

	pipeline.add(s1,s2)
	X = [0] * img.shape[0]*img.shape[1]

	k = 0
	for i in xrange(img.shape[0]):
		for j in xrange(img.shape[1]):
			x = pipeline.apply(img_pad[i:i+size,j:j+size])
			X[k] = x 
			k += 1

	A = detector.predict_proba(X)[:1].reshape(A.shape)

	imshow(A)
	show()

if __name__ == '__main__':
	#read_dataset()
	test_detection_single_image()