import IO
from wrappers import MaxoutWrapper
from pylearn2.utils import serial
from pylab import *


def produce_learning_curves(path):
	model = serial.load(path)
	channels = model.monitor.channels
	t1 = channels['train_y_misclass']
	t2 = channels['test_y_misclass']
	y1 = np.asarray(t1.val_record)
	y2 = np.asarray(t2.val_record)

	x = np.asarray(t1.batch_record)
	plot(x,y1)
	plot(x,y2)
	show()



def produce_confusion_matrix(model):
	
	
	y_hat = model.predict(X)
	hits = (y_hat == y)
	nhits = hits.sum()
	misfires = []
	for i in xrange(len(y)):
		if hits[i] == False:
			misfires.append(X[i])
			
	misfires = np.array(misfires)
	save_dataset(X=misfires)
	show_confusion(y,y_hat)



def save_confusion(y,y_pred):
	cm = confusion_matrix(y, y_pred)
	cm = np.array(cm,'float32')

	cm /= cm.sum(axis=1)[:,np.newaxis]
	IO.pickle(cm,"confusion_matrix.pkl")
	alphabet = ALPHABET
	
	p.matshow(cm)
	p.title('Confusion matrix')
	p.xticks(range(len(alphabet)), alphabet)
	p.yticks(range(len(alphabet)), alphabet)
	p.xlabel('Predicted')
	p.ylabel('Actual')
	p.set_cmap("PuBuGn")
	p.colorbar()	
	p.show()



def produce_plots():
	#model = MaxoutWrapper('yaml/icdar_855.pkl')
	produce_learning_curves('yaml/icdar_855.pkl')

if __name__=='__main__':
	produce_plots()