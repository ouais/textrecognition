import IO
from misc import *
from preprocessing import Pipeline,togray,normalize_sample
from datatypes import WordImage

def create_word_datasets():
	trainpath = words_train_dir
	trainpickle  = words_train_pkl
	
	testpath = words_test_dir
	testpickle = words_test_pkl

	pipeline = Pipeline()	

	s1 = lambda x: [(togray(y[0]),y[1]) for y in x]	
	s2 = lambda x: [(normalize_sample(y[0]),y[1]) for y in x]
	s3 = lambda x: [WordImage(image=y[0],word=y[1]) for y in x]	

	pipeline.add(s1,s2,s3)

	dio = IO.DatasetReader(pipeline=pipeline, files=['all'])
	dio.create_pickle(trainpath,trainpickle)
	dio.create_pickle(testpath,testpickle)

if __name__ == '__main__':
	create_word_datasets()