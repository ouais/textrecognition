import IO
import numpy as np
import scipy
import time
from preprocessing import *
from IO import DatasetReader
from hmms import HMMSegmentor
from misc import *
from pylab import imshow,show,figure
from datatypes import WordImage
from wrappers import MaxoutWrapper
from bisect import bisect
from blist import sortedlist
from Levenshtein import distance
from preprocessing import Pipeline,normalize_sample,togray,resize_to_width,otsu
from lm import *
from wed import weighted_distance


def create_word_recognition_pkl():
	pipeline = Pipeline()
	s1 = lambda x: zip(*x)
	s2 = lambda x: ([togray(t) for t in x[0]],x[1])
	s3 = lambda x: ([normalize_sample(t) for t in x[0]],x[1])

	pipeline.add(s1,s2,s3)

	dio = DatasetReader(pipeline=pipeline, files=['all'])
	dio.create_pickle(word_train_dir,word_train_pkl)

def correct_lexicon(original_results,lexicon):
		y_hat = filter(lambda x: len(x[0]) > 3,original_results)
		if len(y_hat) > 0:
			original_results = y_hat
		i = 0
		minimum = (np.inf,np.inf,np.inf)

		y = [''.join(t for t in x[0]) for x in original_results]

		while i < len(y):				
			z = np.array([distance(y[i],x) for x in lexicon])
			k = z.argmin()
			# print z[k],minimum,y[i]
			if z[k] < minimum[0]:
				minimum = (z[k],k,i)
			i +=1

			if z[k] == 0:
				break

		k = minimum[1]
		
		return (lexicon[k],minimum[0],original_results[minimum[2]],original_results)

class WordRecognizer():
	def __init__(self):
		pass

	def recognize(self,image,with_accuracies=False):
		pass

	def measure_accuracy(self,wordimages,language_models=None):
		hits = 0
		total = len(wordimages)

		for i in xrange(len(wordimages)):
			wordimage = wordimages[i]			
			if language_models != None:
				self.lms = language_models[i]

			y = self.recognize(wordimage)
			y = [''.join(t for t in x[0]) for x in y]

			y_hat = filter(lambda x: len(x) > 2,y)
			if len(y_hat) > 0:
				y = y_hat

			if  wordimage.word == y[0]:
				hits +=1
			else:
				# print i, y[0], wordimage.word
				pass
				# imshow(wordimage.image)
				# show()

		return hits,total,hits*1./total

	def recognize_lexicon(self,wordimage,lexicon):
		
		original_results = self.recognize(wordimage)
		return correct_lexicon(original_results,lexicon)

	def measure_accuracy_lexiconfree(self,wordimages,lexicons):
		hits = 0
		total = len(wordimages)
		self.lms = None

		for i in xrange(len(wordimages)):
		
			result = self.recognize_lexicon(wordimages[i],lexicons[i])[0]

			if  wordimages[i].word == result:
				hits +=1
			else:
				# print i, result,wordimages[i].word
				# imshow(wordimages[i].image,cmap=cm.Greys_r)
				# show()
				pass

		return hits,total,hits*1./total

class Segmentation_WR(WordRecognizer):
	def __init__(self, preprocessor=None,segmentor=None,recognizer=None,detector=None,selector=None,lms=None,lexicon=None):

		WordRecognizer.__init__(self)

		self.preprocessor = preprocessor
		self.segmentor = segmentor
		self.detector = detector
		self.recognizer = recognizer
		self.selector = selector
		self.lms = lms
		self.lexicon = lexicon

		pipeline = Pipeline()
		s1 = lambda x: resize_sample(x,(32,32))
		s2 = lambda x: normalize_sample(x)
		s3 = lambda x: x.flatten()
		pipeline.add(s1,s2)

		self.pipeline = pipeline

	def recognize(self,wordimage):

		if wordimage.widths != None:
			segments = np.cumsum([0]+wordimage.widths)
		else:
			segments = self.segmentor.segment(wordimage.image)
		cascade = Cascade(self.pipeline)

		cascade.build(segments,split=True,image=wordimage.image,detector=self.detector)
		cascade.evaluate(wordimage.image,self.detector,self.recognizer)

		res = self.selector(cascade,lms=self.lms)

		return res


class Interval(object):
	def __init__(self,start,end):
		self.start = start
		self.end = end			
		self.cost = 0
		self.original = False
		self._detection = 0
		self._activation = 0  
		
	@property
	def activation(self):
		return self._activation
	
	@activation.setter
	def activation(self,x):
		self._activation = x
		self.sorted_chars = [(-np.log2(x[1]),ALPHABETi[x[0]]) for x in enumerate(self._activation)]
		self.collapse_capital()
		self.sorted_chars.sort()
		self.char_cost = [np.inf for x in enumerate(self.sorted_chars)]
		self.words = [[] for x in enumerate(self.sorted_chars)]		

	@property
	def detection(self):
		return self._detection	
		
	@detection.setter
	def detection(self, x):
		self._detection = -np.log2(x)			

	def collapse_small(self):
		new_chars = []
		sorted_chars = self.sorted_chars
		for i in xrange(len(sorted_chars)):
			for j in xrange(len(sorted_chars)):
				if sorted_chars[i][1] == sorted_chars[j][1].upper():
					new_chars.append((sorted_chars[j][0]+sorted_chars[i][0],sorted_chars[i][1]))
		self.sorted_chars = new_chars

	def collapse_capital(self):
		new_chars = []
		sorted_chars = self.sorted_chars
		for i in xrange(len(sorted_chars)):
			for j in xrange(len(sorted_chars)):
				if sorted_chars[i][1] == sorted_chars[j][1].lower():
					new_chars.append((sorted_chars[j][0]+sorted_chars[i][0],sorted_chars[i][1]))
		self.sorted_chars = new_chars

	def __repr__(self):
		return '('+str(self.start)+','+str(self.end)+') ' + 'conf: %.2f' % self.detection


class Node:
	def __init__(self,word,cost,interval):
		self.interval = interval
		self.cost = cost
		self.word = word
		self.linguistic_cost = 0
		self.visual_cost = 0

	def __repr__(self):
		return " %.2f" % self.cost +", "+ str(self.word)


class Cascade:
	def __init__(self,pipeline=None):
		self.pipeline= pipeline

	def build(self,segments,mark_originals=False,split=False,image=None,detector=None):

		intervals = []
		originals = []		
		
		if mark_originals:
			for i in xrange(len(segments)-1):
				originals.append((segments[i],segments[i+1]))		

		#print segments

		if detector != None and image != None:
			visual_segments = []
			for i in xrange(len(segments)-2):
				left = self.pipeline.apply(image[:,segments[i]:segments[i+1]])
				right = self.pipeline.apply(image[:,segments[i+1]:segments[i+2]])
				center = self.pipeline.apply(image[:,segments[i]:segments[i+2]])

				visual_segments.append(left)
				visual_segments.append(right)
				visual_segments.append(center)

			visual_segments = np.array(visual_segments)
			predictions = detector.predict_proba(visual_segments)
			
			for i in xrange(len(segments)-2):
				left,right,center = predictions[i*3:3*i+3][:,1]

				if center >= max(left,right):					

					interval = Interval(segments[i],segments[i+2])
		 			intervals.append(interval)

		if split:			
			segments = self.split_segments(segments)
			while len(segments) < 3:
				segments = self.split_segments(segments)

		for step in xrange(1,2):
			for i in xrange(len(segments)-step):				
				interval = Interval(segments[i],segments[i+step])
				intervals.append(interval)

		for step in xrange(2,3):
			for i in xrange(len(segments)-step):				
				if i % 2 == 0:
					interval = Interval(segments[i],segments[i+step])
					intervals.append(interval)
				
		if mark_originals:
			for i in xrange(len(intervals)):
				if (intervals[i].start,intervals[i].end) in originals:
					intervals[i].original = True
					
		self.intervals = intervals
		self.start = min(intervals,key=lambda x: x.start).start
		self.end = max(intervals,key=lambda x: x.end).end
		# self.build_next_graph()
		self.build_prev_graph()
		
	def evaluate(self,image,detector,recognizer):
		ts = []
		for i in xrange(len(self.intervals)):
			interval = self.intervals[i]
			t = self.pipeline.apply(image[:,interval.start:interval.end])
			ts.append(t)

		ts = np.array(ts)

		recognition = recognizer.predict_proba(ts)		
		detection = detector.predict_proba(ts)

		for i in xrange(len(self.intervals)):
			self.intervals[i].activation = recognition[i]
			self.intervals[i].detection = detection[i][1]
			# self.intervals[i].activation = recognizer.predict_proba(t)[0]
			# self.intervals[i].detection = detector.predict_proba(t)[0][1]
		self.image = image


	def reverse(self):		
		for interval in self.intervals:
			interval.start,interval.end  = -interval.end,-interval.start
		self.start = min(self.intervals,key=lambda x: x.start).start
		self.end = max(self.intervals,key=lambda x: x.end).end
		#self.build_next_graph()
		self.build_prev_graph()

	def split_segments(self,segments):
		s = []

		for i in xrange(len(segments)-1):
			s.append(segments[i])
			if segments[i+1]-segments[i] >=4:
				s.append(segments[i]+(segments[i+1]-segments[i])/2)				

		s.append(segments[-1])

		return s

	def build_next_graph(self):

		starts = []
		self.intervals.sort(key=lambda x: x.start)

		for i in xrange(len(self.intervals)):
			starts.append(self.intervals[i].start)

		for i in xrange(len(self.intervals)-1,-1,-1):
			k = bisect(starts,self.intervals[i].end-1)
			self.intervals[i].nexts = []
			self.intervals[i].next = i

			while k < len(self.intervals) and k != i and self.intervals[k].start == self.intervals[i].end:
				self.intervals[i].nexts.append(k)
				k +=1

	def build_prev_graph(self):

		ends = []
		
		self.intervals.sort(key=lambda x: x.end)

		for i in xrange(len(self.intervals)):
			ends.append(self.intervals[i].end)

		#print ends

		for i in xrange(len(self.intervals)-1,-1,-1):
			k = bisect(ends,self.intervals[i].start-1)
			#print self.intervals[i].start,self.intervals[i].end,k
			self.intervals[i].prevs = []
			self.intervals[i].prev = i

			while k < len(self.intervals) and k != i and self.intervals[k].end == self.intervals[i].start:
				self.intervals[i].prevs.append(k)
				k +=1


def qterbi(cascade,B = 5, lms = None,reverse=False):
	intervals = cascade.intervals
	# for i in xrange(len(intervals)):		
	#  	x = intervals[i]
	#    	print i,x.detection,x.prevs,x.start,x.end,max(x.sorted_chars,key=lambda x:x[0])
	 #   	imshow(cascade.image[:,x.start:x.end])
		# show()

	for x in intervals:		
		x.words = sortedlist([],lambda x:x[1])	

		for i in xrange(len(x.sorted_chars)):
			for prev in x.prevs:
				y = intervals[prev]
				for j in xrange(len(y.words)):					
					linguistic_cost = 0

					if lms != None:
						lm_id = min(max(lms),len(y.words[j][0])+1)
						context = [char for char in y.words[j][0][-lm_id+1:]]					
						linguistic_cost = lms[lm_id].logprob(x.sorted_chars[i][1],context)		

						if x.end == cascade.end:
							word = y.words[j][0] + [x.sorted_chars[i][1]]
							lm_id = min(max(lms),len(word)+1)
							context = [char for char in word[-lm_id+1:]]					
							linguistic_cost += lms[lm_id].logprob('',context)	

					visual_cost = x.sorted_chars[i][0] + x.detection
					
					cost = visual_cost + linguistic_cost + y.words[j][1]
					word = y.words[j][0] + [x.sorted_chars[i][1]]

					x.words.add((word,cost,visual_cost+y.words[j][2],linguistic_cost+y.words[j][3]))

		for i in xrange(len(x.sorted_chars)):
			if x.start == cascade.start:
				linguistic_cost = 0

				if lms != None:
					linguistic_cost = lms[2].logprob(x.sorted_chars[i][1],[''])

				cost = x.detection + x.sorted_chars[i][0] + linguistic_cost
				x.words.add((['',x.sorted_chars[i][1]],cost,x.detection + x.sorted_chars[i][0],linguistic_cost))

		x.words = x.words[:B]

	results = sortedlist([],lambda x:x[1])	

	total_cost = np.inf
	for x in intervals:
		if x.end != cascade.end:
			continue

		for i in xrange(len(x.words)):	
				results.add(x.words[i])

	return results

def test_single_word(sr):
	im_path = 'images/20.jpg'
	word = cv2.imread(im_path,cv2.CV_LOAD_IMAGE_COLOR)        
	word = togray(word)
	word = cv2.resize(word,(200,word.shape[0]*200/word.shape[1]))
	#word = word[:,::-1]
	image = word

	image= WordImage(image=image)

	lexicon = ['graphic','giraffe','cow','FLASH']
	lms = get_l2r_lexicon(lexicon,n=2)
	#sr.lms = lms

	x = sr.recognize(image)
	print x

def test_ICDAR_FULL(sr):
	dataset = IO.unpickle(words_test_pkl)
	dataset = filter(lambda x: all((c in ALPHABET) for c in x.word) and len(x.word) >=3,dataset)

	for x in dataset:
	 	x.image = resize_to_width(x.image,200)
	 	x.widths = None
	 	x.word = x.word.lower()
		
	#dataset = dataset[:100]
	
	# lexicon = [x.word for x in dataset]
	# lexicon.extend(IO.unpickle(large_lexicon))
	lexicon = IO.unpickle(large_lexicon)
	lexicon = [x.lower() for x in lexicon]

	lexicons = [lexicon]*len(dataset)

	# lms = get_l2r_lexicon(lexicon,n=4)
	
	result =  sr.measure_accuracy_lexiconfree(dataset,lexicons)
	# print 'ICDAR-FULL with edit-distance: %d,%d,%f' % (result)

	# sr.lms = lms
	# result =  sr.measure_accuracy(dataset)
	print 'ICDAR-FULL with order 5 language model: %d,%d,%f' % (result)

def test_ICDAR_50(sr):
	dataset = IO.unpickle(words_test_pkl)
	lexicons = IO.unpickle(test_words_lex50_pkl)

	print '%d words, %d lexicons' % (len(dataset),len(lexicons))

	dataset = dataset[:len(lexicons)]

	z = zip(dataset,lexicons)

	z = filter(lambda x: all((c in ALPHABET) for c in x[0].word) and len(x[0].word) >=3,z)

	dataset,lexicons = zip(*z)

	print '%d words after removing non-alphanumeric words and words of length < 3' % len(dataset)

	for x in dataset:
	 	x.image = resize_to_width(x.image,200)
	 	x.widths = None
	
	# dataset = dataset[646:700]
	# lexicons = lexicons[646:700]

	lms = [get_l2r_lexicon(x,n=5) for x in lexicons]

	start = time.time()
	result =  sr.measure_accuracy_lexiconfree(dataset,lexicons)
	print 'ICDAR-50 with edit-distance: %d,%d,%f' % (result)
	end = time.time()
	print end-start

	result = sr.measure_accuracy(dataset,lms)
	print 'ICDAR-50 with order 5 language model: %d,%d,%f' % (result)
	

def test_ngram(sr):
	dataset = IO.unpickle(words_test_pkl)
	lexicons_50 = IO.unpickle(test_words_lex50_pkl)

	dataset = dataset[:len(lexicons_50)]

	z = zip(dataset,lexicons_50)

	z = filter(lambda x: all((c in ALPHABET) for c in x[0].word) and len(x[0].word) >=3,z)

	dataset,lexicons_50 = zip(*z)

	for x in dataset:
	 	x.image = resize_to_width(x.image,200)
	 	x.widths = None
	 	# x.word = x.word.lower()
		
	#dataset = dataset[:100]
	# lexicon_full = [x.word for x in dataset]
	lexicon_large = IO.unpickle(large_lexicon)
	# lexicon_large.extend(lexicon_full)

	for n in xrange(2,9):
		# print 'ICDAR-50'
		# lms = [get_l2r_lexicon(x,n=n) for x in lexicons_50]		
		sr.lms = get_l2r_lexicon(lexicon_large,n=n)
		print n, sr.measure_accuracy(dataset)

def test_SVT(sr):
	dataset = IO.unpickle(SVT_test_words_pkl)
	lexicons = IO.unpickle(SVT_test_words_lexicons_pkl)

	print '%d words, %d lexicons' % (len(dataset),len(lexicons))

	z = zip(dataset,lexicons)

	z = filter(lambda x: all((c in ALPHABET) for c in x[0].word) and len(x[0].word) >=3,z)

	dataset,lexicons = zip(*z)

	# dataset = dataset[10:11]
	# lexicons = lexicons[10:11]

	print '%d words after removing non-alphanumeric words and words of length < 3' % len(dataset)

	for x in dataset:
	 	x.image = resize_to_width(x.image,200)
	 	x.widths = None
	
	result = sr.measure_accuracy_lexiconfree(dataset,lexicons)
	print 'SVT with edit-distance: %d,%d,%f' % (result)

	lms = [get_l2r_lexicon(lexicon,n=5) for lexicon in lexicons]
	result = sr.measure_accuracy(dataset,lms)
	print 'SVT with language  order 5 model: %d,%d,%f' % (result)


if __name__=='__main__':
	#segmentation_pkl = 'maxout_natural_hmm_10.pkl'
	segmentation_pkl = 'models/MaxoutWrapper_10_hmm_1.pkl'
	#segmentation_pkl = 'models/Maxout_height_1.pkl'
	#segmentation_pkl = 'models/ExtraTreesClassifier_10_hmm_1.pkl'
	#recognizer = IO.unpickle('recognizer_SVC.pkl')

	hs = HMMSegmentor(segmentation_pkl)
	
	recognizer = MaxoutWrapper('yaml/icdar_855.pkl')
	detector = MaxoutWrapper('yaml/segmentation_corrector.pkl')

	sr = Segmentation_WR(segmentor=hs,detector=detector,recognizer=recognizer,selector=qterbi)
	
	
	# test_SVT(sr)
	# test_ICDAR_50(sr)
	# test_ICDAR_FULL(sr)
	test_ngram(sr)	
	#test_single_word(sr)

	

