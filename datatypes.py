from collections import namedtuple


class TaggedRectangle:
    def __init__(self,tag=None, attribs= None):
        self.tag = tag
        if attribs is not None: 
            # things are reversed because thats the way a matrix is accessed
            self.y = max(int(float(attribs['x'])),0)
            self.x = max(int(float(attribs['y'])),0)
            self.width = int(float(attribs['width']))
            self.height = int(float(attribs['height']))

            self.xmin = self.x
            self.xmax = self.x + self.height

            self.ymin = self.y
            self.ymax = self.y + self.width
        else:
            self.x = None
            self.y = None
            self.width = None
            self.height = None

    def __repr__(self):
        return '(%d,%d) ,(%d,%d)' % (self.xmin,self.xmax,self.ymin,self.ymax)


class TaggedImage:

    def __init__(self,image=None,tagged_rectangles=None,lexicon=None):
        self.image = image
        self.tagged_rectangles = tagged_rectangles
        self.lexicon = lexicon


class WordImage(object):
    def __init__(self, image=None,word=None,segments=None,widths=None):
        self.image = image
        self.word = word
        self.segments = segments
        self.widths = widths

