import numpy as np

def simulate(n,limit):
	s = 0
	for i in xrange(limit):
		a = np.random.randint(0,2,n)
		a = np.array(np.where(a==0,1,-1),dtype=float)				
		s += (a.sum())**8

	s /= limit
	s /= n**4
	return s