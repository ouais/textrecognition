
import theano
import theano.tensor as T
import numpy as np

class HMM:
	def __init__(self, n_components=1, startprob=None, transmat=None,
				 startprob_prior=None, transmat_prior=None,
				 algorithm="viterbi", random_state=None,
				 n_iter=10, thresh=1e-2, params=string.ascii_letters,
				 init_params=string.ascii_letters):

		self.n_components = n_components
		self.n_iter = n_iter
		self.thresh = thresh
		self.params = params
		self.init_params = init_params
		self.startprob_ = startprob
		self.startprob_prior = startprob_prior
		self.transmat_ = transmat
		self.transmat_prior = transmat_prior
		self._algorithm = algorithm
		self.random_state = random_state

		emission_tensor = T.dmatrix('emission')

		#Expression for extracting the right row out of the emission matrix 
		extract_e = emission_tensor[obs_type] * T.eye(T.shape(emission_tensor)[1], m = T.shape(emission_tensor)[1])

		self._log_transmat_ = T.dmatrix('log_transition_mat')
		self._log_startrob = T.dvector('log_startprob')

		final_probs_tensor = T.dvector('final_probs')

	def eval(self, obs):
		"""Compute the log probability under the model and compute posteriors

		Parameters
		----------
		obs : array_like, shape (n, n_features)
			Sequence of n_features-dimensional data points.  Each row
			corresponds to a single point in the sequence.

		Returns
		-------
		logprob : float
			Log likelihood of the sequence `obs`
		posteriors: array_like, shape (n, n_components)
			Posterior probabilities of each state for each
			observation

		See Also
		--------
		score : Compute the log probability under the model
		decode : Find most likely state sequence corresponding to a `obs`
		"""
		obs = np.asarray(obs)
		framelogprob = self._compute_log_likelihood(obs)
		logprob, fwdlattice = self._do_forward_pass(framelogprob)
		bwdlattice = self._do_backward_pass(framelogprob)
		gamma = fwdlattice + bwdlattice
		# gamma is guaranteed to be correctly normalized by logprob at
		# all frames, unless we do approximate inference using pruning.
		# So, we will normalize each frame explicitly in case we
		# pruned too aggressively.
		posteriors = np.exp(gamma.T - logsumexp(gamma, axis=1)).T
		posteriors += np.finfo(np.float32).eps
		posteriors /= np.sum(posteriors, axis=1).reshape((-1, 1))
		return logprob, posteriors

	def score(self, obs):
		"""Compute the log probability under the model.

		Parameters
		----------
		obs : array_like, shape (n, n_features)
			Sequence of n_features-dimensional data points.  Each row
			corresponds to a single data point.

		Returns
		-------
		logprob : float
			Log likelihood of the `obs`

		See Also
		--------
		eval : Compute the log probability under the model and posteriors
		decode : Find most likely state sequence corresponding to a `obs`
		"""
		obs = np.asarray(obs)
		framelogprob = self._compute_log_likelihood(obs)
		logprob, _ = self._do_forward_pass(framelogprob)
		return logprob

	def _decode_viterbi(self, obs):
		"""Find most likely state sequence corresponding to `obs`.

		Uses the Viterbi algorithm.

		Parameters
		----------
		obs : array_like, shape (n, n_features)
			List of n_features-dimensional data points.  Each row
			corresponds to a single data point.

		Returns
		-------
		viterbi_logprob : float
			Log probability of the maximum likelihood path through the HMM
		state_sequence : array_like, shape (n,)
			Index of the most likely states for each observation

		See Also
		--------
		eval : Compute the log probability under the model and posteriors
		score : Compute the log probability under the model
		"""
		obs = np.asarray(obs)
		framelogprob = self._compute_log_likelihood(obs)
		viterbi_logprob, state_sequence = self._do_viterbi_pass(framelogprob)
		return viterbi_logprob, state_sequence

	def _decode_map(self, obs):
		"""Find most likely state sequence corresponding to `obs`.

		Uses the maximum a posteriori estimation.

		Parameters
		----------
		obs : array_like, shape (n, n_features)
			List of n_features-dimensional data points.  Each row
			corresponds to a single data point.

		Returns
		-------
		map_logprob : float
			Log probability of the maximum likelihood path through the HMM
		state_sequence : array_like, shape (n,)
			Index of the most likely states for each observation

		See Also
		--------
		eval : Compute the log probability under the model and posteriors
		score : Compute the log probability under the model
		"""
		_, posteriors = self.eval(obs)
		state_sequence = np.argmax(posteriors, axis=1)
		map_logprob = np.max(posteriors, axis=1).sum()
		return map_logprob, state_sequence

	def decode(self, obs, algorithm="viterbi"):
		"""Find most likely state sequence corresponding to `obs`.
		Uses the selected algorithm for decoding.

		Parameters
		----------
		obs : array_like, shape (n, n_features)
			List of n_features-dimensional data points.  Each row
			corresponds to a single data point.

		algorithm : string, one of the `decoder_algorithms`
			decoder algorithm to be used

		Returns
		-------
		logprob : float
			Log probability of the maximum likelihood path through the HMM
		state_sequence : array_like, shape (n,)
			Index of the most likely states for each observation

		See Also
		--------
		eval : Compute the log probability under the model and posteriors
		score : Compute the log probability under the model
		"""
		if self._algorithm in decoder_algorithms:
			algorithm = self._algorithm
		elif algorithm in decoder_algorithms:
			algorithm = algorithm
		decoder = {"viterbi": self._decode_viterbi,
				   "map": self._decode_map}
		logprob, state_sequence = decoder[algorithm](obs)
		return logprob, state_sequence

	def predict(self, obs, algorithm="viterbi"):
		"""Find most likely state sequence corresponding to `obs`.

		Parameters
		----------
		obs : array_like, shape (n, n_features)
			List of n_features-dimensional data points.  Each row
			corresponds to a single data point.

		Returns
		-------
		state_sequence : array_like, shape (n,)
			Index of the most likely states for each observation
		"""
		_, state_sequence = self.decode(obs, algorithm)
		return state_sequence

	def predict_proba(self, obs):
		"""Compute the posterior probability for each state in the model

		Parameters
		----------
		obs : array_like, shape (n, n_features)
			List of n_features-dimensional data points.  Each row
			corresponds to a single data point.

		Returns
		-------
		T : array-like, shape (n, n_components)
			Returns the probability of the sample for each state in the model.
		"""
		_, posteriors = self.eval(obs)
		return posteriors

	def sample(self, n=1, random_state=None):
		"""Generate random samples from the model.

		Parameters
		----------
		n : int
			Number of samples to generate.

		random_state: RandomState or an int seed (0 by default)
			A random number generator instance. If None is given, the
			object's random_state is used

		Returns
		-------
		(obs, hidden_states)
		obs : array_like, length `n` List of samples
		hidden_states : array_like, length `n` List of hidden states
		"""
		if random_state is None:
			random_state = self.random_state
		random_state = check_random_state(random_state)

		startprob_pdf = self.startprob_
		startprob_cdf = np.cumsum(startprob_pdf)
		transmat_pdf = self.transmat_
		transmat_cdf = np.cumsum(transmat_pdf, 1)

		# Initial state.
		rand = random_state.rand()
		currstate = (startprob_cdf > rand).argmax()
		hidden_states = [currstate]
		obs = [self._generate_sample_from_state(
			currstate, random_state=random_state)]

		for _ in range(n - 1):
			rand = random_state.rand()
			currstate = (transmat_cdf[currstate] > rand).argmax()
			hidden_states.append(currstate)
			obs.append(self._generate_sample_from_state(
				currstate, random_state=random_state))

		return np.array(obs), np.array(hidden_states, dtype=int)

	def fit(self, obs):
		"""Estimate model parameters.

		An initialization step is performed before entering the EM
		algorithm. If you want to avoid this step, pass proper
		``init_params`` keyword argument to estimator's constructor.

		Parameters
		----------
		obs : list
			List of array-like observation sequences (shape (n_i, n_features)).

		Notes
		-----
		In general, `logprob` should be non-decreasing unless
		aggressive pruning is used.  Decreasing `logprob` is generally
		a sign of overfitting (e.g. a covariance parameter getting too
		small).  You can fix this by getting more training data, or
		decreasing `covars_prior`.
		"""

		if self.algorithm not in decoder_algorithms:
			self._algorithm = "viterbi"

		self._init(obs, self.init_params)

		logprob = []
		for i in range(self.n_iter):
			# Expectation step
			stats = self._initialize_sufficient_statistics()
			curr_logprob = 0
			for seq in obs:
				framelogprob = self._compute_log_likelihood(seq)
				lpr, fwdlattice = self._do_forward_pass(framelogprob)
				bwdlattice = self._do_backward_pass(framelogprob)
				gamma = fwdlattice + bwdlattice
				posteriors = np.exp(gamma.T - logsumexp(gamma, axis=1)).T
				curr_logprob += lpr
				self._accumulate_sufficient_statistics(
					stats, seq, framelogprob, posteriors, fwdlattice,
					bwdlattice, self.params)
			logprob.append(curr_logprob)

			# Check for convergence.
			if i > 0 and abs(logprob[-1] - logprob[-2]) < self.thresh:
				break

			# Maximization step
			self._do_mstep(stats, self.params)

		return self

	def _get_algorithm(self):
		"decoder algorithm"
		return self._algorithm

	def _set_algorithm(self, algorithm):
		if algorithm not in decoder_algorithms:
			raise ValueError("algorithm must be one of the decoder_algorithms")
		self._algorithm = algorithm

	algorithm = property(_get_algorithm, _set_algorithm)

	def _get_startprob(self):
		"""Mixing startprob for each state."""
		return np.exp(self._log_startprob)

	def _set_startprob(self, startprob):
		if startprob is None:
			startprob = np.tile(1.0 / self.n_components, self.n_components)
		else:
			startprob = np.asarray(startprob, dtype=np.float)

		# check if there exists a component whose value is exactly zero
		# if so, add a small number and re-normalize
		if not np.alltrue(startprob):
			normalize(startprob)

		if len(startprob) != self.n_components:
			raise ValueError('startprob must have length n_components')
		if not np.allclose(np.sum(startprob), 1.0):
			raise ValueError('startprob must sum to 1.0')

		self._log_startprob = np.log(np.asarray(startprob).copy())

	startprob_ = property(_get_startprob, _set_startprob)

	def _get_transmat(self):
		"""Matrix of transition probabilities."""
		return np.exp(self._log_transmat)

	def _set_transmat(self, transmat):
		if transmat is None:
			transmat = np.tile(1.0 / self.n_components,
							   (self.n_components, self.n_components))

		# check if there exists a component whose value is exactly zero
		# if so, add a small number and re-normalize
		if not np.alltrue(transmat):
			normalize(transmat, axis=1)

		if (np.asarray(transmat).shape
				!= (self.n_components, self.n_components)):
			raise ValueError('transmat must have shape '
							 '(n_components, n_components)')
		if not np.all(np.allclose(np.sum(transmat, axis=1), 1.0)):
			raise ValueError('Rows of transmat must sum to 1.0')

		self._log_transmat = np.log(np.asarray(transmat).copy())
		underflow_idx = np.isnan(self._log_transmat)
		self._log_transmat[underflow_idx] = NEGINF

	transmat_ = property(_get_transmat, _set_transmat)

	def _do_viterbi_pass(self, framelogprob):
		n_observations, n_components = framelogprob.shape
		state_sequence, logprob = _hmmc._viterbi(
			n_observations, n_components, self._log_startprob,
			self._log_transmat, framelogprob)
		return logprob, state_sequence

	def _do_forward_pass(self, framelogprob):

		n_observations, n_components = framelogprob.shape
		fwdlattice = np.zeros((n_observations, n_components))
		_hmmc._forward(n_observations, n_components, self._log_startprob,
					   self._log_transmat, framelogprob, fwdlattice)
		fwdlattice[fwdlattice <= ZEROLOGPROB] = NEGINF
		return logsumexp(fwdlattice[-1]), fwdlattice

	def _do_backward_pass(self, framelogprob):
		n_observations, n_components = framelogprob.shape
		bwdlattice = np.zeros((n_observations, n_components))
		_hmmc._backward(n_observations, n_components, self._log_startprob,
						self._log_transmat, framelogprob, bwdlattice)

		bwdlattice[bwdlattice <= ZEROLOGPROB] = NEGINF

		return bwdlattice

	def _compute_log_likelihood(self, obs):
		pass

	def _generate_sample_from_state(self, state, random_state=None):
		pass

	def _init(self, obs, params):
		if 's' in params:
			self.startprob_.fill(1.0 / self.n_components)
		if 't' in params:
			self.transmat_.fill(1.0 / self.n_components)

	# Methods used by self.fit()

	def _initialize_sufficient_statistics(self):
		stats = {'nobs': 0,
				 'start': np.zeros(self.n_components),
				 'trans': np.zeros((self.n_components, self.n_components))}
		return stats

	def _accumulate_sufficient_statistics(self, stats, seq, framelogprob,
										  posteriors, fwdlattice, bwdlattice,
										  params):
		stats['nobs'] += 1
		if 's' in params:
			stats['start'] += posteriors[0]
		if 't' in params:
			n_observations, n_components = framelogprob.shape
			lneta = np.zeros((n_observations - 1, n_components, n_components))
			lnP = logsumexp(fwdlattice[-1])
			_hmmc._compute_lneta(n_observations, n_components, fwdlattice,
								 self._log_transmat, bwdlattice, framelogprob,
								 lnP, lneta)
			stats["trans"] += np.exp(logsumexp(lneta, 0))

	def _do_mstep(self, stats, params):
		# Based on Huang, Acero, Hon, "Spoken Language Processing",
		# p. 443 - 445
		if self.startprob_prior is None:
			self.startprob_prior = 1.0
		if self.transmat_prior is None:
			self.transmat_prior = 1.0

		if 's' in params:
			self.startprob_ = normalize(
				np.maximum(self.startprob_prior - 1.0 + stats['start'], 1e-20))
		if 't' in params:
			transmat_ = normalize(
				np.maximum(self.transmat_prior - 1.0 + stats['trans'], 1e-20),
				axis=1)
			self.transmat_ = transmat_