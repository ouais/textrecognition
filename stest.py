import numpy as np
from pylab import *

if __name__=='__main__':
	a = np.array([1,3,5,10,15,20,25,30,100])
	b = a*3+2
	plot(a,b,'bo',a,b,'k')
	xlabel('Beam Width')
	ylabel('Seconds per Query')
	
	show()

