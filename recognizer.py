import IO
import cv2
import preprocessing as pp
from preprocessing import *
from pylab import *
import numpy as np
import matplotlib.pyplot as plt


class CharRecognizer():

	def __init__(self):
		self.pipeline = None
		self.classifier= None


	def classify(self,char):
		temp = self.pipeline.apply(char)
		return self.classifier.predict(temp)


if __name__ == '__main__':
	pass
