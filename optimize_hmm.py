import IO
from misc import *
from hmms import left_to_right_discrimnative, HMMSegmentor,segmentation_transition
from hybridhmm import Hybrid_Discrimnative_HMM
from estimators import WindowEstimator
from test_word_recognition import beamsearch,Segmentation_WR


def optimize_words_segmentation_hmm():
	#dataset = IO.unpickle(synthetic_words_pkl)
	dataset = IO.unpickle(natural_words_pkl)
	#test_set = IO.unpickle(words_test_pkl)
	#test_set = test_set[:400]
	dataset = dataset[:500]

	n_classes = 2
	spc = 4
	frame_width = 10

	A,pi = segmentation_transition(spc)
	model = Hybrid_Discrimnative_HMM(n_classes=n_classes,spc=spc,transmat=A,startprob=pi,n_iter=5,post_m=save_hmm)
	model.estimator = WindowEstimator(n_classes=n_classes*spc,frame_width=frame_width,evaluate=True)
	model.name = model.estimator.pc.classifier.__class__ .__name__ +'_'+str(frame_width)+'_words_hmm'
	model.fit(dataset)

def save_hmm(model):
	IO.pickle(model,'models/'+model.name+'_'+str(model.iterations)+'.pkl')


def optimize_lines_segmentation_hmm():
	dataset = IO.unpickle(icdar_line_segmentation_pkl)
	dataset = filter(lambda x: len(x.segments) != 1,dataset)

	n_classes = 2
	spc = 4
	frame_width = 40

	A,pi = segmentation_transition(spc)
	model = Hybrid_Discrimnative_HMM(n_classes=n_classes,spc=spc,transmat=A,startprob=pi,n_iter=5,post_m=save_hmm)
	model.estimator = WindowEstimator(n_classes=n_classes*spc,frame_width=frame_width,evaluate=True)
	model.name = model.estimator.pc.classifier.__class__ .__name__ +'_'+str(frame_width)+'_lines_hmm'
	model.fit(dataset)

def test_hmm(dataset):

	#recognizer = MaxoutWrapper('yaml/icdar_834.pkl')
	recognizer = IO.unpickle('recognizer_SVC.pkl')
	detector = IO.unpickle('detector_svm.pkl')

	def f(hmm):
		hs = HMMSegmentor()
		hs.model = hmm
		sr = Segmentation_WR(segmentor=hs,detector=detector,recognizer=recognizer,selector=beamsearch)
		print sr.measure_accuracy(dataset)
		IO.pickle(model,fname)

	return f


if __name__ == '__main__':
	#optimize_segmentation_hmm()
	optimize_lines_segmentation_hmm()