import IO
import numpy as np
import scipy
from preprocessing import *
from IO import DatasetReader
from hmms import HMMSegmentor
from misc import *
from pylab import imshow,show,figure
from datatypes import WordImage
from wrappers import MaxoutWrapper
from bisect import bisect
from blist import sortedlist
from Levenshtein import distance
from preprocessing import Pipeline,normalize_sample,togray,resize_to_width,otsu
from lm import *
from wed import weighted_distance

def create_word_recognition_pkl():
	pipeline = Pipeline()
	s1 = lambda x: zip(*x)
	s2 = lambda x: ([togray(t) for t in x[0]],x[1])
	s3 = lambda x: ([normalize_sample(t) for t in x[0]],x[1])

	pipeline.add(s1,s2,s3)

	dio = DatasetReader(pipeline=pipeline, files=['all'])
	dio.create_pickle(word_train_dir,word_train_pkl)

class WordRecognizer():
	def __init__(self):
		pass

	def recognize(self,image):
		pass

	def measure_accuracy(self,wordimages,language_models=None,lexicons=None):
		hits = 0
		total = len(wordimages)

		for i in xrange(len(wordimages)):
			wordimage = wordimages[i]			
			if language_models != None:
				self.lms = language_models[i]
				
			y = self.recognize(wordimage)

			if  wordimage.word in y:
				hits +=1
			else:
				print y, wordimage.word
				pass
				# imshow(wordimage.image)
				# show()

		return hits,total,hits*1./total

	def measure_accuracy_lexiconfree(self,wordimages,lexicons):
		hits = 0
		total = len(wordimages)
		self.lms = None

		for i in xrange(len(wordimages)):
			wordimage = wordimages[i]			

			if lexicons != None:
				self.lexicon = lexicons[i]

			y = self.recognize(wordimage)
			i = 0

			minimum = (np.inf,np.inf)

			while i < len(y):				
				z = np.array([distance(y[i],x) for x in self.lexicon])
				k = z.argmin()
				if z[k] < minimum[0]:
					minimum = (z[k],k)

				i +=1

				if z[k] == 0:
					break
			k = minimum[1]

			if  wordimage.word == self.lexicon[k]:
				hits +=1
			else:
				print self.lexicon[k],wordimage.word
				pass

		return hits,total,hits*1./total

class Segmentation_WR(WordRecognizer):
	def __init__(self, preprocessor=None,segmentor=None,recognizer=None,detector=None,selector=None,lms=None,lexicon=None):

		WordRecognizer.__init__(self)

		self.preprocessor = preprocessor
		self.segmentor = segmentor
		self.detector = detector
		self.recognizer = recognizer
		self.selector = selector
		self.lms = lms
		self.lexicon = lexicon

		pipeline = Pipeline()
		s1 = lambda x: resize_sample(x,(32,32))
		s2 = lambda x: normalize_sample(x)
		s3 = lambda x: x.flatten()
		pipeline.add(s1,s2)

		self.pipeline = pipeline

	def recognize(self,wordimage):
		if wordimage.widths != None:
			segments = np.cumsum([0]+wordimage.widths)
		else:
			segments = self.segmentor.segment(wordimage.image)
		cascade = Cascade(self.pipeline)

		cascade.build(segments,split=True,image=wordimage.image,detector=self.detector)
		cascade.evaluate(wordimage.image,self.detector,self.recognizer)

		res = self.selector(cascade,lms=self.lms)

		return res


class Interval(object):
	def __init__(self,start,end):
		self.start = start
		self.end = end			
		self.cost = 0
		self.original = False
		self._detection = 0
		self._activation = 0  
		
	@property
	def activation(self):
		return self._activation
	
	@activation.setter
	def activation(self,x):
		self._activation = x
		self.sorted_chars = [(-np.log2(x[1]),ALPHABETi[x[0]]) for x in enumerate(self._activation)]
		self.collapse()
		self.sorted_chars.sort()
		self.char_cost = [np.inf for x in enumerate(self.sorted_chars)]
		self.words = [[] for x in enumerate(self.sorted_chars)]		

	@property
	def detection(self):
		return self._detection	
		
	@detection.setter
	def detection(self, x):
		self._detection = -np.log2(x)			

	def collapse(self):
		new_chars = []
		sorted_chars = self.sorted_chars
		for i in xrange(len(sorted_chars)):
			for j in xrange(len(sorted_chars)):
				if sorted_chars[i][1] == sorted_chars[j][1].upper():
					new_chars.append((sorted_chars[j][0]+sorted_chars[i][0],sorted_chars[i][1]))
		self.sorted_chars = new_chars

	def __repr__(self):
		return '('+str(self.start)+','+str(self.end)+') ' + 'conf: %.2f' % self.detection


class Node:
	def __init__(self,word,cost,interval):
		self.interval = interval
		self.cost = cost
		self.word = word
		self.linguistic_cost = 0
		self.visual_cost = 0

	def __repr__(self):
		#return str(self.interval) +", " + " %.2f" % self.cost +", "+ str(self.word)
		return " %.2f" % self.cost +", "+ str(self.word)


class Cascade:
	def __init__(self,pipeline=None):
		self.pipeline= pipeline

	def build(self,segments,mark_originals=False,split=False,image=None,detector=None):

		intervals = []
		originals = []		
		
		if mark_originals:
			for i in xrange(len(segments)-1):
				originals.append((segments[i],segments[i+1]))		

		#print segments

		if detector != None and image != None:
			for i in xrange(len(segments)-2):								
				left = self.pipeline.apply(image[:,segments[i]:segments[i+1]])
				right = self.pipeline.apply(image[:,segments[i+1]:segments[i+2]])
				center = self.pipeline.apply(image[:,segments[i]:segments[i+2]])

				left = detector.predict_proba(left)[0][1]
				right = detector.predict_proba(right)[0][1]
				center = detector.predict_proba(center)[0][1]

				#print left,right,center,segments[i],segments[i+1],segments[i+2]
				if center >= max(left,right):					

					interval = Interval(segments[i],segments[i+2])
		 			intervals.append(interval)

		if split:			
			segments = self.split_segments(segments)
			while len(segments) < 3:
				segments = self.split_segments(segments)

		for step in xrange(1,2):
			for i in xrange(len(segments)-step):				
				interval = Interval(segments[i],segments[i+step])
				intervals.append(interval)

		for step in xrange(2,3):
			for i in xrange(len(segments)-step):				
				if i % 2 == 0:
					interval = Interval(segments[i],segments[i+step])
					intervals.append(interval)
				
		if mark_originals:
			for i in xrange(len(intervals)):
				if (intervals[i].start,intervals[i].end) in originals:
					intervals[i].original = True
					
		self.intervals = intervals
		self.start = min(intervals,key=lambda x: x.start).start
		self.end = max(intervals,key=lambda x: x.end).end
		self.build_next_graph()
		self.build_prev_graph()
		
	def evaluate(self,image,detector,recognizer):
		
		for i in xrange(len(self.intervals)):
			interval = self.intervals[i]
			# scipy.misc.imsave('paper_images/c%d.png' % i,image[:,interval.start:interval.end])
			# imshow(image[:,interval.start:interval.end])
			# show()
			t = self.pipeline.apply(image[:,interval.start:interval.end])
			self.intervals[i].activation = recognizer.predict_proba(t)[0]
			self.intervals[i].detection = detector.predict_proba(t)[0][1]
		self.image = image


	def reverse(self):		
		for interval in self.intervals:
			interval.start,interval.end  = -interval.end,-interval.start
		self.start = min(self.intervals,key=lambda x: x.start).start
		self.end = max(self.intervals,key=lambda x: x.end).end
		#self.build_next_graph()
		self.build_prev_graph()

	def split_segments(self,segments):
		s = []

		for i in xrange(len(segments)-1):
			s.append(segments[i])
			if segments[i+1]-segments[i] >=4:
				s.append(segments[i]+(segments[i+1]-segments[i])/2)				

		s.append(segments[-1])

		return s

	def build_next_graph(self):

		starts = []
		self.intervals.sort(key=lambda x: x.start)

		for i in xrange(len(self.intervals)):
			starts.append(self.intervals[i].start)

		for i in xrange(len(self.intervals)-1,-1,-1):
			k = bisect(starts,self.intervals[i].end-1)
			self.intervals[i].nexts = []
			self.intervals[i].next = i

			while k < len(self.intervals) and k != i and self.intervals[k].start == self.intervals[i].end:
				self.intervals[i].nexts.append(k)
				k +=1

	def build_prev_graph(self):

		ends = []
		
		self.intervals.sort(key=lambda x: x.end)

		for i in xrange(len(self.intervals)):
			ends.append(self.intervals[i].end)

		#print ends

		for i in xrange(len(self.intervals)-1,-1,-1):
			k = bisect(ends,self.intervals[i].start-1)
			#print self.intervals[i].start,self.intervals[i].end,k
			self.intervals[i].prevs = []
			self.intervals[i].prev = i

			while k < len(self.intervals) and k != i and self.intervals[k].end == self.intervals[i].start:
				self.intervals[i].prevs.append(k)
				k +=1


def viterbi(cascade,lms = None):
	intervals = cascade.intervals
	#intervals.sort(key=lambda x: x.start)

	# for i in xrange(len(intervals)):		
	#  	x = intervals[i]
	#    	print i,x.detection,x.prevs,x.start,x.end,max(x.sorted_chars,key=lambda x:x[0])
	  	#print x.sorted_chars
		# imshow(cascade.image[:,x.start:x.end])
		# show()

	for x in intervals:		
		for i in xrange(len(x.sorted_chars)):
			for prev in x.prevs:
				y = intervals[prev]
				for j in xrange(len(y.sorted_chars)):					
					linguistic_cost = 0

					if lms != None:
						lm_id = min(max(lms),len(y.words[j])+1)
						context = [char for char in y.words[j][-lm_id+1:]]					
						linguistic_cost = lms[lm_id].logprob(x.sorted_chars[i][1],context)		

						if x.end == cascade.end:
							word = y.words[j] + [x.sorted_chars[i][1]]
							lm_id = min(max(lms),len(word)+1)
							context = [char for char in word[-lm_id+1:]]					
							linguistic_cost += lms[lm_id].logprob('',context)	

					visual_cost = x.sorted_chars[i][0] + x.detection
					
					cost = visual_cost + linguistic_cost + y.char_cost[j]

					if cost < x.char_cost[i]:
						x.char_cost[i] = cost
						x.words[i] = y.words[j] + [x.sorted_chars[i][1]]

			 		# if x.sorted_chars[i][1] == 'A':
			  	#  	  	print context, y.words[j],x.end,linguistic_cost,lm_id,visual_cost,cost
			# if x.sorted_chars[i][1] == 'C':
			#    	print ''.join(z for z in x.words[i]), x.char_cost[i]

		for i in xrange(len(x.sorted_chars)):
			if x.char_cost[i] == np.inf and x.start == cascade.start:
				linguistic_cost = 0

				if lms != None:
					linguistic_cost = lms[2].logprob(x.sorted_chars[i][1],[''])

				x.char_cost[i] = x.detection + x.sorted_chars[i][0] + linguistic_cost
				x.words[i] = ['',x.sorted_chars[i][1]]

	total_cost = np.inf
	for x in intervals:
		if x.end != cascade.end:
			continue

		for i in xrange(len(x.words)):		

			cost = x.char_cost[i]
			#cost /= len(x.words[i])

			# print ''.join(c for c in x.words[i]), cost
			if cost < total_cost:

				total_cost = cost
				soln = x.words[i]
				s_id = (x,i)

	#print s_id[0].words[s_id[1]],s_id[0].start,s_id[0].end
	print total_cost

	return ''.join(x for x in soln)



def qterbi(cascade,B = 100, lms = None,reverse=False):
	intervals = cascade.intervals
	# for i in xrange(len(intervals)):		
	#  	x = intervals[i]
	#    	print i,x.detection,x.prevs,x.start,x.end,max(x.sorted_chars,key=lambda x:x[0])
	 #   	imshow(cascade.image[:,x.start:x.end])
		# show()

	for x in intervals:		
		x.words = sortedlist([],lambda x:x[1])	

		for i in xrange(len(x.sorted_chars)):
			for prev in x.prevs:
				y = intervals[prev]
				for j in xrange(len(y.words)):					
					linguistic_cost = 0

					if lms != None:
						lm_id = min(max(lms),len(y.words[j][0])+1)
						context = [char for char in y.words[j][0][-lm_id+1:]]					
						linguistic_cost = lms[lm_id].logprob(x.sorted_chars[i][1],context)		

						if x.end == cascade.end:
							word = y.words[j][0] + [x.sorted_chars[i][1]]
							lm_id = min(max(lms),len(word)+1)
							context = [char for char in word[-lm_id+1:]]					
							linguistic_cost += lms[lm_id].logprob('',context)	

					visual_cost = x.sorted_chars[i][0] + x.detection
					
					cost = visual_cost + linguistic_cost + y.words[j][1]
					word = y.words[j][0] + [x.sorted_chars[i][1]]

					x.words.add((word,cost,visual_cost+y.words[j][2],linguistic_cost+y.words[j][3]))

		for i in xrange(len(x.sorted_chars)):
			if x.start == cascade.start:
				linguistic_cost = 0

				if lms != None:
					linguistic_cost = lms[2].logprob(x.sorted_chars[i][1],[''])

				cost = x.detection + x.sorted_chars[i][0] + linguistic_cost
				x.words.add((['',x.sorted_chars[i][1]],cost,x.detection + x.sorted_chars[i][0],linguistic_cost))

		x.words = x.words[:B]

	results = sortedlist([],lambda x:x[1])	

	total_cost = np.inf
	for x in intervals:
		if x.end != cascade.end:
			continue

		for i in xrange(len(x.words)):	
			if len(x.words[i][0]) > 3:
				results.add(x.words[i])

	#results  = [(''.join(y for y in x[0]),x[1]) for x in results]

	results  = [''.join(y for y in x[0]) for x in results]
	
	#results = results[:B]
	#print results
	if reverse:
		results = [(x[0][::-1],x[1]) for x in results]
	# print results
	# answer = {}
	# answer.update(results)
	if results == []:
		return ['errorzzzz']

	return results

def beamsearch(cascade,B=100,n_results=30,min_length=0,lms=None,reverse=False):

	intervals = cascade.intervals

	queues = {}
	queues[2] = sortedlist([],lambda x:x.cost)
	results = {}

	# imshow(cascade.image)
	# show()

	for interval in intervals:
		#print interval,interval.nexts,interval.detection
		#print interval.sorted_chars
		if interval.start == cascade.start:			
			for c in interval.sorted_chars:				
				visual_cost = c[0] + interval.detection				
				linguistic_cost = lms[2].logprob(c[0],[''])	if lms != None else 0	

				cost = visual_cost+linguistic_cost

				if len(queues[2]) < B or queues[2][-1].cost > cost:
					n = Node(['',c[1]],c[0],interval)
					n.linguistic_cost = linguistic_cost
					n.visual_cost = visual_cost
					queues[2].add(n)
				if len(queues[2]) > B:
					del queues[2][-1]

	while True:
		# for x in queues:
		# 	print x,": ",queues[x]
		index = -1
		min_cost = np.inf
		for i in queues:			
			if len(queues[i]) > 0 and queues[i][0].cost < min_cost:
				index = i
				min_cost = queues[i][0].cost
		if index == -1:
			return []
		x = queues[index][0]
		del queues[index][0]

		if x.interval.end == cascade.end:
			w = ''.join([y for y in x.word])
			if reverse:
				w = w[::-1]

			results[w] = x
			#print w,x.cost
			if len(results) == n_results:
				break

		for j in x.interval.nexts:
			interval = intervals[j]

			try:
				q = queues[len(x.word)+1]
			except:
				queues[len(x.word)+1] = sortedlist([],lambda x:x.cost)
				q = queues[len(x.word)+1]			

			for char_logprob,char in interval.sorted_chars:

				visual_cost = char_logprob + interval.detection
				linguistic_cost = 0

				if lms != None:
					lm_id = min(max(lms),len(x.word)+1)
					context = [y for y in x.word[-lms[lm_id]._n+1:len(x.word)]]
					linguistic_cost = lms[lm_id].logprob(char,context)	
				
				cost = visual_cost+ linguistic_cost +x.cost 

				word = x.word+ [char]

				if len(q) < B or q[-1].cost > cost: #c[0] is probability, c[1] is character
					u = Node(word,cost,interval)
					u.prev = x
					u.visual_cost = x.visual_cost + visual_cost
					u.linguistic_cost = x.linguistic_cost + linguistic_cost
					q.add(u)
				if len(q) > B:
					del q[-1]

	# results = [(k,results[k].cost) for k in results]
	# results.sort(key=lambda x: x[1])
	# print results

	return results

def test_single_word(sr):
	im_path = 'images/20.jpg'
	word = cv2.imread(im_path,cv2.CV_LOAD_IMAGE_COLOR)        
	word = togray(word)
	word = cv2.resize(word,(200,word.shape[0]*200/word.shape[1]))
	#word = word[:,::-1]
	image = word

	image= WordImage(image=image)

	lexicon = ['graphic','giraffe','cow','FLASH']
	lms = get_l2r_lexicon(lexicon,n=2)
	#sr.lms = lms

	x = sr.recognize(image)
	print x

def test_dataset(sr):
	#dataset = IO.unpickle(natural_words_pkl)
	dataset = IO.unpickle(words_test_pkl)
	dataset = filter(lambda x: all((c in ALPHABET) for c in x.word) and len(x.word) >=3,dataset)

	for x in dataset:
	 	x.image = resize_to_width(x.image,200)
	 	x.widths = None
		
	#dataset = dataset[:100]
	
	lexicon = [x.word for x in dataset]
	lms = get_l2r_lexicon(lexicon,n=5)

	sr.lms = lms

	print sr.measure_accuracy(dataset)

def test_ICDAR_50(sr):
	dataset = IO.unpickle(words_test_pkl)
	lexicons = IO.unpickle(test_words_lex50_pkl)

	print '%d words, %d lexicons' % (len(dataset),len(lexicons))

	dataset = dataset[:len(lexicons)]

	z = zip(dataset,lexicons)

	z = filter(lambda x: all((c in ALPHABET) for c in x[0].word) and len(x[0].word) >=3,z)

	dataset,lexicons = zip(*z)

	print '%d words after removing non-alphanumeric words and words of length < 3' % len(dataset)

	for x in dataset:
	 	x.image = resize_to_width(x.image,200)
	 	x.widths = None
	
	print sr.measure_accuracy_lexiconfree(dataset,lexicons)

def test_SVT(sr):
	dataset = IO.unpickle(SVT_test_words_pkl)
	lexicons = IO.unpickle(SVT_test_words_lexicons_pkl)

	print '%d words, %d lexicons' % (len(dataset),len(lexicons))

	z = zip(dataset,lexicons)

	z = filter(lambda x: all((c in ALPHABET) for c in x[0].word) and len(x[0].word) >=3,z)

	dataset,lexicons = zip(*z)

	dataset = dataset[10:11]
	lexicons = lexicons[10:11]

	print '%d words after removing non-alphanumeric words and words of length < 3' % len(dataset)

	for x in dataset:
	 	x.image = resize_to_width(x.image,200)
	 	x.widths = None
	
	result = sr.measure_accuracy_lexiconfree(dataset,lexicons)
	print 'SVT with edit-distance: %d,%d,%f' % (result)

	lms = [get_l2r_lexicon(lexicon,n=5) for lexicon in lexicons]
	result = sr.measure_accuracy(dataset,lms)
	print 'SVT with language  order 5 model: %d,%d,%f' % (result)

if __name__=='__main__':
	#segmentation_pkl = 'maxout_natural_hmm_10.pkl'
	segmentation_pkl = 'models/MaxoutWrapper_10_hmm_1.pkl'
	#segmentation_pkl = 'models/Maxout_height_1.pkl'
	#segmentation_pkl = 'models/ExtraTreesClassifier_10_hmm_1.pkl'
	#recognizer = IO.unpickle('recognizer_SVC.pkl')

	hs = HMMSegmentor(segmentation_pkl)
	
	recognizer = MaxoutWrapper('yaml/icdar_855.pkl')
	detector = MaxoutWrapper('yaml/segmentation_corrector.pkl')

	sr = Segmentation_WR(segmentor=hs,detector=detector,recognizer=recognizer,selector=qterbi)
	
	test_SVT(sr)
	# test_dataset(sr)
	# test_ICDAR_50(sr)
	#test_ICDAR_FULL(sr)
	#test_single_word(sr)

	
