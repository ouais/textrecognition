import numpy as np
import string
from sklearn.hmm import _BaseHMM
from sklearn.hmm import normalize
from estimators import SequencedImage
from pylab import *
from datetime import datetime

def safelog(posterior):
	for i in xrange(posterior.shape[0]):
		for j in xrange(posterior.shape[1]):
			if posterior[i,j] <= 0:
				posterior[i,j] = -1e20
			else:
				posterior[i,j] = np.log2(posterior[i,j])
	return posterior

class HybridHMM(_BaseHMM):
	"""Hidden Markov Model with multinomial (discrete) emissions

	Attributes
	----------
	n_components : int
		Number of states in the model.

	n_symbols : int
		Number of possible symbols emitted by the model (in the observations).

	transmat : array, shape (`n_components`, `n_components`)
		Matrix of transition probabilities between states.

	startprob : array, shape ('n_components`,)
		Initial state occupation distribution.

	emissionprob : array, shape ('n_components`, 'n_symbols`)
		Probability of emitting a given symbol when in each state.

	random_state: RandomState or an int seed (0 by default)
		A random number generator instance

	n_iter : int, optional
		Number of iterations to perform.

	thresh : float, optional
		Convergence threshold.

	params : string, optional
		Controls which parameters are updated in the training
		process.  Can contain any combination of 's' for startprob,
		't' for transmat, 'm' for means, and 'c' for covars, etc.
		Defaults to all parameters.

	init_params : string, optional
		Controls which parameters are initialized prior to
		training.  Can contain any combination of 's' for
		startprob, 't' for transmat, 'm' for means, and 'c' for
		covars, etc.  Defaults to all parameters.

	Examples
	--------
	>>> from sklearn.hmm import MultinomialHMM
	>>> MultinomialHMM(n_components=2)
	...                             #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE
	MultinomialHMM(algorithm='viterbi',...

	See Also
	--------
	GaussianHMM : HMM with Gaussian emissions
	"""

	def __init__(self, n_components=1, startprob=None, transmat=None,
				 startprob_prior=None, transmat_prior=None,
				 algorithm="viterbi", random_state=None,mask=None,post_m=None,
				 n_iter=10, thresh=1e-2, params=string.ascii_letters,
				 init_params=string.ascii_letters):
		"""Create a hidden Markov model with multinomial emissions.

		Parameters
		----------
		n_components : int
			Number of states.
		"""
		_BaseHMM.__init__(self, n_components, startprob, transmat,
						  startprob_prior=startprob_prior,
						  transmat_prior=transmat_prior,
						  algorithm=algorithm,
						  random_state=random_state,
						  n_iter=n_iter,
						  thresh=thresh,
						  params=params,
						  init_params=init_params)



		self.estimator = None
		self.firstpass = True
		self.iterations = 0
		self.likelihood = 0
		self.likelihoods = []
		self.mask = mask
		self.post_m = post_m
		self.print_likelihood = False		
		if self.mask == None:
			self.mask = np.ones([n_components,n_components],dtype = int)	
	
	def _get_transmat(self):
		"""Matrix of transition probabilities."""
		return np.exp(self._log_transmat)


	def _set_transmat(self, transmat):

		transmat /=transmat.sum(axis=1)[:,np.newaxis]		
		
		if (np.asarray(transmat).shape
				!= (self.n_components, self.n_components)):
			raise ValueError('transmat must have shape '
							 '(n_components, n_components)')

		np.set_printoptions(precision=3,suppress=True)
		#print transmat

		if not np.all(np.allclose(np.sum(transmat, axis=1), 1.0)):
			raise ValueError('Rows of transmat must sum to 1.0')

		self._log_transmat = safelog(np.asarray(transmat).copy())
		underflow_idx = np.isnan(self._log_transmat)
		self._log_transmat[underflow_idx] = -np.inf

	transmat_ = property(_get_transmat,_set_transmat)


	def decode(self, obs):
		framelogprob = self._compute_log_likelihood(obs)
		viterbi_logprob, state_sequence = self._do_viterbi_pass(framelogprob)
		return viterbi_logprob, state_sequence


	def _compute_firstpass_posteriors(self,obs):

		z = [int(np.ceil(obs.shape[1]/(self.n_components+1)*i)) for i in xrange(self.n_components+1)]
		z[-1] = obs.shape[1]
		posteriors = np.zeros([obs.shape[1],self.n_components])
		for x in xrange(self.n_components):
			posteriors[z[x]:z[x+1],x] = 2

		return posteriors

	def _compute_log_likelihood(self, obs):
		if self.firstpass:
			posteriors = self._compute_firstpass_posteriors(obs)
		else:			
			posteriors = self.estimator.predict(obs)
			posteriors /= self.priors

		posteriors = self._augment_posteriors(posteriors)

		return safelog(posteriors)
		
	def _augment_posteriors(self,posteriors):
		pass

	def _init(self, obs, params='ste'):
		super(HybridHMM, self)._init(obs, params=params)

	def _initialize_sufficient_statistics(self):
		stats = super(HybridHMM, self)._initialize_sufficient_statistics()
		stats['priors'] = np.ones(self.n_components,dtype=int)
		stats['seqs'] = []
		return stats

	def _accumulate_sufficient_statistics(self, stats, obs, framelogprob,
										  posteriors, fwdlattice, bwdlattice,
										  params):
		super(HybridHMM, self)._accumulate_sufficient_statistics(
			stats, obs, framelogprob, posteriors, fwdlattice, bwdlattice,
			params)

		if 'e' in params:
			viterbi_seq = self.decode(obs)[1]			
			for x in viterbi_seq:
				stats['priors'][x] += 1

			if len(viterbi_seq) == 1+ obs.length:
				viterbi_seq = np.delete(viterbi_seq,-1,0)

			if self.print_likelihood:
				lpr, fwdlattice = self._do_forward_pass(framelogprob)
				self.likelihood += lpr
				

			stats['seqs'].append((obs,viterbi_seq))
			#print stats['trans']

	def _do_mstep(self, stats, params):

		super(HybridHMM, self)._do_mstep(stats, params)		
		self.transmat_ = self.transmat_ * self.mask	

		score = 0
		if 'e' in params:
			score = self.estimator.update(stats['seqs'])

			stats['seqs']= []
			self.priors = np.array(stats['priors'][:],dtype=float)
			self.priors /= self.priors.sum()
			stats['priors'] = np.ones(self.n_components,dtype=int)

		self.firstpass = False
		self.iterations +=1
			
		status = "EM iteration %d over. Classifier training score: %.3f" % (self.iterations, score)

		if self.print_likelihood:
			self.likelihoods.append(self.likelihood)
			status += " with Likelihood: %.3f" % self.likelihood
			self.likelihood = 0
		
		print status

		if self.post_m != None:
			self.post_m(self)

class Hybrid_Discrimnative_HMM(HybridHMM):

	def __init__(self, n_components=1, n_classes=2, spc=3,startprob=None, transmat=None,
				 startprob_prior=None, transmat_prior=None,
				 algorithm="viterbi", random_state=None,mask=None,post_m=None,caching=True,
				 n_iter=10, thresh=1e-2, params=string.ascii_letters,
				 init_params=string.ascii_letters):


		HybridHMM.__init__(self, spc*n_classes, startprob, transmat,
						  startprob_prior=startprob_prior,
						  transmat_prior=transmat_prior,
						  algorithm=algorithm,
						  random_state=random_state,
						  n_iter=n_iter,
						  thresh=thresh,
						  mask=mask,
						  params=params,
						  post_m=post_m,
						  init_params=init_params)

		self.args = locals()
		self.n_classes = n_classes
		self.spc = spc
		self.caching = caching

		r = max(self.spc/3,1)
		pad = np.array([0]*(self.spc-r) +[1]*r)
		self.pad = np.tile(pad,self.n_classes)

	def _augment_posteriors(self,posteriors):
		return np.vstack((posteriors,self.pad))


	# def _compute_firstpass_posteriors(self,obs):
	# 	"this was made for words"
	# 	word = obs.labels
	# 	num_slices = obs.length

	# 	z = [num_slices*i/len(word) for i in xrange(len(word)+1)]

	# 	posteriors = np.zeros([num_slices,self.n_components])

	# 	for i in xrange(len(word)):
	# 		m = _create_firstpass_matrix(z[i+1]-z[i],self.spc) 
	# 		posteriors[z[i]:z[i+1],word[i]*self.spc:(word[i]+1)*self.spc] = m

	# 	return posteriors


	def _compute_firstpass_posteriors(self,obs):
		"""
		used to compute the first pass posterior in case of samples with segments
		"""
		num_slices = obs.length
		posteriors = np.zeros([num_slices,self.n_components])

		prev = 0
		for seg in obs.labels:
			m = _create_firstpass_matrix(seg[0],self.spc)
			#print m.shape,seg[0],prev,len(posteriors)
			posteriors[prev:prev+seg[0],seg[1]*self.spc:(seg[1]+1)*self.spc] = m
			prev += seg[0]

		return posteriors

	def decode_to_classes(self,obs):
		seq = self.decode(obs)
		seq = seq[1] # because decode returns a log likelihood + sequence
		clamp = [x/self.spc for x in seq]
		t = filter(lambda x: clamp[x-1] != clamp[x],xrange(1,len(clamp)))
		clamp = [clamp[0]] + [clamp[x] for x in t]
		return clamp	

	def segment(self,obs):		
		seq = self.decode(obs)
		seq = seq[1] # because decode returns a log likelihood + sequence
		clamp = [x/self.spc for x in seq]
		return clamp

	def fit(self, obs, **kwargs):
		obs = self._normalize_dataset(obs)
		HybridHMM.fit(self, obs, **kwargs)
		
	def _normalize_dataset(self,X):
		print 'caching images...'
		new_X = [0]*len(X)
		
		for i in xrange(len(X)):
			new_X[i] = SequencedImage(X[i].image,X[i].segments,self.estimator,self.caching)
			
		print 'caching complete.'
		return new_X

def _create_firstpass_matrix(height,width):
		z = [height*i/width for i in xrange(width+1)]
		m = np.zeros([height,width])
		for x in xrange(width):
			m[z[x]:z[x+1],x] = 2
		return m





