import IO
import cv2
import numpy as np
from random import shuffle
from preprocessing import togray,segment
from pylab import *
from collections import Counter,namedtuple
from sklearn.metrics import confusion_matrix
from sklearn.ensemble import ExtraTreesClassifier,AdaBoostClassifier,GradientBoostingClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC,LinearSVC
from deepnn.dA import dA
from misc import *


Box = namedtuple('Box', ['x1', 'y1','x2','y2','depth','label'], verbose=False)

class Feature:
	def __init__(self,image):
		self.original = image
		self.image = segment(image)
		self.image = self.correct_labels()
		self.boxes = self.build_quadtree()
		self.boxes = self.combine_boxes()
		self.points = self.build_function()

	def evaluate(self,image):
		res = 0
		for point,label in self.points:
			res += image[point]*label
		return res

	def correct_labels(self):
		image = self.image
		mini = image.min()
		maxi = image.max()
		
		for i in xrange(len(image)):
			for j in xrange(len(image)):
				if image[i,j] == mini:
					image[i,j] = -1
				elif image[i,j] == maxi:
					image[i,j] = 1		
		return image

	def build_quadtree(self,threshold=.9,max_depth=3):

		image = self.image
		h,w = image.shape

		q = []
		q.append(Box(0,0,h,w,0,0))

		boxes = []

		while len(q) != 0:
			cur = q[0]
			del q[0]

			x1,y1,x2,y2 = cur.x1,cur.y1,cur.x2,cur.y2

			c = Counter(image[x1:x2,y1:y2].flatten())
			x = c.most_common(1)[0]
			if x[1] >= (x2-x1+1)*(y2-y1+1)*threshold or cur.depth == max_depth:
				box = Box(x1,y1,x2,y2,cur.depth,int(x[0]))
				boxes.append(box)
			else:
				q.append(Box(x1,y1,(x1+x2)/2,(y1+y2)/2,cur.depth+1,0))
				q.append(Box((x1+x2)/2,y1,x2,(y1+y2)/2,cur.depth+1,0))
				q.append(Box(x1,(y1+y2)/2,(x1+x2)/2,y2,cur.depth+1,0))
				q.append(Box((x1+x2)/2,(y1+y2)/2,x2,y2,cur.depth+1,0))	

		return boxes

	def share_a_side(self,box1,box2):
		a = box1.x2 == box2.x2 and box1.x1 == box2.x1 and (box1.y2 == box2.y1 or box1.y1 == box2.y2)		
		b = box1.y2 == box2.y2 and box1.y1 == box2.y1 and (box1.x2 == box2.x1 or box1.x1 == box2.x2)		
		return a or b
		
	def merge_boxes(self,box1,box2):
		x1 = min(box1.x1,box2.x1)
		y1 = min(box1.y1,box2.y1)
		x2 = max(box1.x2,box2.x2)
		y2 = max(box1.y2,box2.y2)

		return Box(x1,y1,x2,y2,box1.depth,box1.label)

	def combine_boxes(self):
		boxes = self.boxes

		check = True
		while check:
			check = False
			for i in xrange(len(boxes)):
				for j in xrange(i+1,len(boxes)):
					if boxes[i].label == boxes[j].label and self.share_a_side(boxes[i],boxes[j]):
						a = boxes[i]
						b = boxes[j]

						del boxes[j]
						del boxes[i]
						boxes.append(self.merge_boxes(a,b))
						check = True
						break
				if check:
					break
		return boxes

	def build_function(self):
		boxes = self.boxes
		points = {-1:[],1:[]}

		for box in boxes:
			if not box.label in [-1,1]:
				continue
			points[box.label].append((box.x2-1,box.y2-1))
			points[-box.label].append((box.x1-1,box.y2-1))
			points[-box.label].append((box.x2-1,box.y1-1))
			points[box.label].append((box.x1-1,box.y1-1))

		c1 = Counter(points[1])
		c2 = Counter(points[-1])

		for x in c2:
			if x in c1:
				c1[x] -= c2[x]
			else:
				c1[x] = -c2[x]

		result = filter(lambda x: x[0] >= 0 and x[1] >= 0,c1)
		result = [(x,c1[x]) for x in result]
		return result

class CascadeClassifier:
	def __init__(self,f,d,F_target):
		self.f = f
		self.d = d
		self.F_target = F_target
		self.size = 32

		self.cascade = []
		self.thresholds = []
		self.features = []

	def feed(self,img):
		for i in xrange(len(self.cascade)):
			classifier = self.cascade[i]
			x = [0]*len(self.features[i])
			for j in xrange(len(self.features[i])):
				x[j] = self.features[i][j].evaluate(img)

			r = classifier.predict_proba(x)[:,1]
			if r < self.thresholds[i]:
				return 0

		return 1


	def detect_single_scale(self,img):
		II = integrate(img)
		r = np.zeros(img.shape)
		for i in xrange(img.shape[0]-self.size):
			for j in xrange(img.shape[1]-self.size):
				r[i,j] = self.feed(img[i:i+self.size,j:j+self.size])
		return r


	def predict(self,X):
		y_hat = np.ones(len(X),dtype=int)
		indices = range(len(X))
		X = np.array(X)

		for i in xrange(len(self.cascade)):
			classifier = self.cascade[i]

			new_X = convert_dataset(X,self.features[i])
			y_t = classifier.predict_proba(new_X[indices])[:,1]

			indices_n = np.where(y_t < self.thresholds[i])
			indices = np.where(y_t >= self.thresholds[i])
			
			y_hat[indices_n] = 0

		return y_hat

	def tune_cascade(self,X,y,D_prev):
		print 'tuning cascade...'
		D = 0.
		self.thresholds.append(.5)

		while D < self.d*D_prev:	
			print self.thresholds		
			y_hat = self.predict(X)
			cm = confusion_matrix(y, y_hat)
			cm = np.array(cm,'float32')
			cm /= cm.sum(axis=1)[:,np.newaxis]
			F,D = cm[0,1],cm[1,1]

			if D < self.d*D_prev:
				self.thresholds[-1] -= .1

		indices = np.array([x for x in xrange(len(y)) if y[x] == 0 and y_hat[x] == 1])
		N = [(X[i],0) for i in indices]
		return F,D,N




	def fit(self,X,y):
		X = np.array(X)
		z = zip(X,y)
		P = [x for x in z if x[1] == 1]
		N = [x for x in z if x[1] == 0]

		F = 1.0
		D = 1.0

		i = 0
		while F > self.F_target:
			i += 1
			n = 0
			F_prev = F

			print 'fitting stage %d...' % i
			cycle = False

			while F > self.f*F_prev:
				n += 2
				print 'cycling with %d features' % n 
				if cycle:
					del self.cascade[-1]
					del self.features[-1]
					del self.thresholds[-1]

				
				X_star,y_star = zip(*P)
				X_star,y_star = list(X_star),list(y_star)
				x2,y2 = zip(*N)				
				x2,y2 = list(x2),list(y2)

				X_star.extend(x2)
				y_star.extend(y2)				
				da = dA(1024,n)
				da.fit(X_star,y_star)

				W = da.W.get_value(borrow=True).T
				features = [Feature(x.reshape(32,32)) for x in W]

				X_hat = convert_dataset(X_star,features)
				visualize(X_hat,y_star)

				cl = GradientBoostingClassifier(max_features=1,max_depth=n)
				
				#cl = AdaBoostClassifier()
				cl.fit(X_hat,y_star)

				self.cascade.append(cl)
				self.features.append(features)

				F,D_hat,NN = self.tune_cascade(X,y,D)
				cycle = True

				print F,D_hat,self.thresholds
			D = D_hat
			N = NN

			print 'stage %d done with D:%.2f F:%.2f threshold: %f' % (i,D,F,self.thresholds[-1])


def test_quadtree(image):
	f = Feature(image)
	z = np.zeros(image.shape)

	for box in f.boxes:
		z[box.x1:box.x2,box.y1:box.y2] = box.label

	matshow(z)
	show()

def convert_dataset(X,features):
	size = 32	
	new_X = np.zeros((len(X),len(features)))

	for i in xrange(len(X)):
		x = X[i]
		z = x.reshape(size,size)
		z = integrate(z)
		new_X[i] = [f.evaluate(z) for f in features]

	return np.array(new_X)

def integrate(I):
	II = np.zeros(I.shape)
	for i in xrange(I.shape[0]):
		for j in xrange(I.shape[1]):
			if i == 0 and j == 0:
				II[i,j] = I[i,j]
			if i == 0 and j > 0:
				II[i,j] = II[i,j-1] + I[i,j]
			if i > 0 and j == 0:
				II[i,j] = II[i-1,j] + I[i,j]
			if i > 0 and j > 0:
				II[i,j] = II[i,j-1] + II[i-1,j] - II[i-1,j-1] + I[i,j]
	return II

def visualize(X,y):
	z = zip(X,y)
	z0 = np.array([x[0] for x in z if x[1] == 0])
	z1 = np.array([x[0] for x in z if x[1] == 1])

	print z0.mean(),z1.mean()
	hist(z0)
	show()
	hist(z1)
	show()


def test_detector_single_image():
	cc = IO.unpickle('cc.pkl')
	I = cv2.imread('images/s3.jpg')
	I = togray(I)
	r = cc.detect_single_scale(I)
	imshow(r)
	show()

def test_VJ():
	X,y =IO.unpickle(data_dir+"detector/detection_set.pkl")
	cc = CascadeClassifier(f=.8,d=.998,F_target=1e-2)
	z = zip(X,y)
	shuffle(z)
	z = z[:1000]
	X,y = zip(*z)

	cc.fit(X,y)
	IO.pickle(cc,'cc.pkl')

def test_feature(p8):
	# a = IO.unpickle('deepnn/denoising_da.pkl')
	# W = da.W.get_value(borrow=True).T
	# p8 = W[0].reshape(size,size)

	x = np.random.randint(0,255,1024)
	x.resize(32,32)

	f = Feature(p8)	
	print f.points	
	s = 0 
	for box in f.boxes:
		if box.label in [-1, 1]:
			s += x[box.x1:box.x2,box.y1:box.y2].sum()*box.label
	print s

	I = x
	II = np.zeros(I.shape)
	for i in xrange(I.shape[0]):
		for j in xrange(I.shape[1]):
			if i == 0 and j == 0:
				II[i,j] = I[i,j]
			if i == 0 and j > 0:
				II[i,j] = II[i,j-1] + I[i,j]
			if i > 0 and j == 0:
				II[i,j] = II[i-1,j] + I[i,j]
			if i > 0 and j > 0:
				II[i,j] = II[i,j-1] + II[i-1,j] - II[i-1,j-1] + I[i,j]

	print f.evaluate(II)

if __name__=='__main__':
	test_VJ()
	#test_feature()
	test_detector_single_image()